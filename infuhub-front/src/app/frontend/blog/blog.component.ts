import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

    constructor() { }

    mySlideOptions3 = [
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
        ['30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam'],
    ];
    mySlideOptions = { items: 3, dots: true, nav: false, loop: true, autoplay: true, navspeed: 500, autoplayTimeout:2500,autoplayHoverPause:true };


    blogFeature = [
        [
            'assets/front_end/images/blog-pic1.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ],
        [
            'assets/front_end/images/blog-pic2.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ],
        [
            'assets/front_end/images/blog-pic3.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ],
        [
            'assets/front_end/images/blog-pic4.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ],
        [
            'assets/front_end/images/blog-pic5.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ],
        [
            'assets/front_end/images/blog-pic6.jpg',
            '30 April 2019', '10:30PM',
            'Lorem ipsum dolor sit amet,consectetur',
            'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
        ]


    ]

    ngOnInit() {

    }

}
