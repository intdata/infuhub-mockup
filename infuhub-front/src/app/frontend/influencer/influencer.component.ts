import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-influencer',
  templateUrl: './influencer.component.html',
  styleUrls: ['./influencer.component.scss']
})
export class InfluencerComponent implements OnInit {

  constructor() { }

  //@ViewChild('slideshow') slideshow: any;

  mySlideImages = [
    'assets/front_end/images/brand-icon1.png',
    'assets/front_end/images/brand-icon2.png',
    'assets/front_end/images/brand-icon3.png',
    'assets/front_end/images/brand-icon4.png',
    'assets/front_end/images/brand-icon5.png',
    'assets/front_end/images/brand-icon1.png',
    'assets/front_end/images/brand-icon2.png',
    'assets/front_end/images/brand-icon3.png',
    'assets/front_end/images/brand-icon4.png'
];
mySlideOptions={items: 5, dots: false, nav: true, loop: true, autoplay: true};

mySlideImages1 = [
  ['assets/front_end/images/infbottom-pic1.jpg','John Smith','Artist'],
  ['assets/front_end/images/infbottom-pic2.jpg','Jasmine M','Artist'],
  ['assets/front_end/images/infbottom-pic3.jpg','Bob Den','Blogger'],
  ['assets/front_end/images/infbottom-pic1.jpg','John Smith','Artist'],
  ['assets/front_end/images/infbottom-pic2.jpg','Jasmine M','Artist'],
  ['assets/front_end/images/infbottom-pic3.jpg','Bob Den','Blogger']
];
  mySlideOptions1={items: 3, dots: false, nav: true, loop: true, autoplay: true};


  mySlideImages2 = [
    ['assets/front_end/images/infbottom-pic4.jpg','Rebecca D','Entertainer'],
    ['assets/front_end/images/infbottom-pic5.jpg','Stewart','Blogger'],
    ['assets/front_end/images/infbottom-pic4.jpg','Rebecca D','Entertainer'],
    ['assets/front_end/images/infbottom-pic5.jpg','Stewart','Blogger'],
    ['assets/front_end/images/infbottom-pic4.jpg','Rebecca D','Entertainer'],
    ['assets/front_end/images/infbottom-pic5.jpg','Stewart','Blogger']
  ];
    mySlideOptions2={items: 2, dots: false, nav: true, loop: true, autoplay: true};


  ngOnInit() {
    
      //this.slideshow.goToSlide(4);

  }

}
