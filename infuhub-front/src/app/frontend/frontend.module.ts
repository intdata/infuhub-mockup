import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontendRoutingModule } from './frontend-routing.module';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CountoModule }  from 'angular2-counto';
import { HomeTwoComponent } from './home-two/home-two.component';
import { HomeThreeComponent } from './home-three/home-three.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { ResourcesComponent } from './resources/resources.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
import { InfluencerComponent } from './influencer/influencer.component';

import { OwlModule } from 'ngx-owl-carousel';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginInfluencerComponent } from './login-influencer/login-influencer.component';
import { ContactusComponent } from './contactus/contactus.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { EbookComponent } from './resources/ebook/ebook.component';
import { CaseStudiesComponent } from './resources/case-studies/case-studies.component';

@NgModule({
  imports: [
    CommonModule,
    FrontendRoutingModule,
    CountoModule,
    FormsModule,
    ReactiveFormsModule,
    OwlModule
  ],
  declarations: [ 
    HomeLayoutComponent, 
    HomeComponent, 
    HeaderComponent, 
    FooterComponent, 
    HomeTwoComponent, 
    HomeThreeComponent, 
    LoginComponent, 
    SignupComponent, 
    ForgotPasswordComponent, 
    ResourcesComponent, 
    PricingComponent, 
    BlogComponent, 
    InfluencerComponent, BlogDetailsComponent, AboutUsComponent, LoginInfluencerComponent, ContactusComponent, TestimonialsComponent, SitemapComponent, PrivacyPolicyComponent, EbookComponent, CaseStudiesComponent
  ]
})
export class FrontendModule { }
