import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../must-match.validator';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { filter } from 'rxjs/operators';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    influenceRegisterForm: FormGroup;
    brandRegForm: FormGroup;
    submitted:boolean = false;
    brandSubmitted:boolean = false;
    userType:number = 0;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {

        // to get the param and select registration form
        this.activatedRoute
            .queryParams
            .subscribe(params => {
                if(params['type'] === "influencer"){
                    this.userType = 1;
                }
            });
      
        this.influenceRegisterForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            influnaireAgree: ['']
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('password', 'confirmPassword')
        });

        this.brandRegForm = this.formBuilder.group({
            companyName: ['', Validators.required],
            countryName: ['', [Validators.required]],
            industryName: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required]],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            title: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            brandAgree: ['']
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('password', 'confirmPassword')
        });

        // set the default values for the fields
        this.influenceRegisterForm.controls["influnaireAgree"].setValue(false);

        this.brandRegForm.controls["countryName"].setValue('');
        this.brandRegForm.controls["industryName"].setValue('');
        this.brandRegForm.controls["brandAgree"].setValue(false);
    }

    // convenience getter for easy access to form fields
    get i() { return this.influenceRegisterForm.controls; }
    get b() { return this.brandRegForm.controls; }

    influenceSubmit() {
    
        this.submitted = true;

        // stop here if form is invalid
        if (this.influenceRegisterForm.invalid) {
            return;
        }

        if(!this.influenceRegisterForm.get('influnaireAgree').value){
            alert('You have to agree privacy policy');
            return;
        }

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.influenceRegisterForm.value));
        this.submitted = false;

    }

    brandSubmit() {
        this.brandSubmitted = true;

        // stop here if form is invalid
        if (this.brandRegForm.invalid) {
            return;
        }

        if(!this.brandRegForm.get('brandAgree').value){
            alert('You have to agree privacy policy');
            return;
        }

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.brandRegForm.value));

        this.brandSubmitted = true;
    }


}
