import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  public show:boolean = false;
  status: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  editCompanyInfo(){
    this.show = true;
    this.status = !this.status;  
  }
  editPersonalInfo(){
    this.show = true;
    this.status = !this.status;  
  }

}
