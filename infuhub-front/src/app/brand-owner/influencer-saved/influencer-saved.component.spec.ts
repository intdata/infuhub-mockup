import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencerSavedComponent } from './influencer-saved.component';

describe('InfluencerSavedComponent', () => {
  let component: InfluencerSavedComponent;
  let fixture: ComponentFixture<InfluencerSavedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencerSavedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencerSavedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
