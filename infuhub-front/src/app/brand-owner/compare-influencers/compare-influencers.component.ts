import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-compare-influencers',
  templateUrl: './compare-influencers.component.html',
  styleUrls: ['./compare-influencers.component.scss']
})
export class CompareInfluencersComponent implements OnInit {
  public totalCompareArr: Array<any> = [];
  public totalCompare: Array<any> = [];
  public expandCampaign: boolean;
  @ViewChild('expanFixd') expanFixd: ElementRef;
  constructor(
    private route: ActivatedRoute,
    private renderer: Renderer2,
  ) { 
    this.expandCampaign = false;
  }

  ngOnInit() {
    
    // console.log(this.route.snapshot.paramMap.get("length"))
    const totalCompareCount = Number(this.route.snapshot.paramMap.get("length"));
    for (let i = 0; i < totalCompareCount; i++) {
      this.totalCompareArr.push({
        campaign: false
      });
    }
  }

  addTocompare(indx) {
    this.totalCompareArr[indx].campaign = true;
    this.totalCompare = this.totalCompareArr.filter(x => x.campaign === true);
  }

  openCampaign() {
    if (!this.expandCampaign && this.totalCompare.length > 0) {
      this.renderer.addClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = true;
    } else {
      this.renderer.removeClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = false;
    }
  }
}
