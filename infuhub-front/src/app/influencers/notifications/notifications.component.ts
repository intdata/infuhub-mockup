import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  public notificationArr: Array<any> = [];
  public checkdLength;
  constructor() { }

  ngOnInit() {
    this.staticValueSet();
  }
  staticValueSet() {
    for (let i = 0; i < 6; i++) {
      this.notificationArr.push({
        checked: false,
        remove: false
      });
    }
  }
  checkCheckbox(indx) {
    !this.notificationArr[indx].checked;
    this.checkdLength = this.notificationArr.filter(x => x.checked === true).length;
  }
  checkAllCheckbox(_event) {
    this.notificationArr.map(i=>{
      i.checked = _event.target.checked;
    })
    this.checkdLength = this.notificationArr.filter(x => x.checked === true).length;
  }
  trashNotification(){
    this.notificationArr.map(i=>{
      i.remove = i.checked;
    })
    this.checkdLength = 0;
  }

  get filterByRemove() {
    return this.notificationArr.filter( x => !x.remove);
  }

}
