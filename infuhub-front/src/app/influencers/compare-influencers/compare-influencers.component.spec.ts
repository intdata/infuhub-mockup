import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareInfluencersComponent } from './compare-influencers.component';

describe('CompareInfluencersComponent', () => {
  let component: CompareInfluencersComponent;
  let fixture: ComponentFixture<CompareInfluencersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareInfluencersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareInfluencersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
