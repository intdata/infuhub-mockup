import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit {

  @ViewChild('doughnutCanvas') doughnutCanvas: ElementRef;
  @ViewChild('doughnutCanvas1') doughnutCanvas1: ElementRef;

  // doughnut
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = false;
  public doughnutChartPlugins = [];

  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[];
  public doughnutChartData: SingleDataSet;

  public doughnutChartOptions1: ChartOptions = {};
  public doughnutChartData1: SingleDataSet;

  // area line chart configuration
  public lineChartData: ChartDataSets[];
  public lineChartData1: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    elements: {
        line: {
            tension: 0
        }
    },
    scales:{
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#f4725d',
      backgroundColor: 'rgba(242, 89, 64, 0.1)',
    },
  ];
  public lineChartColors1: Color[] = [
    {
      borderColor: '#c91e9a',
      backgroundColor: 'rgba(201, 30, 154, 0.1)',
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit() {
    this.setDounatTotalFollow();
    this.setDounatTotalEngage();
    this.setLineYotubeProgress();
    this.setLineInstagramProgress();
  }

    /**
   * set doughnut chart for
   * Total Follow
   */
  setDounatTotalFollow() {
    this.doughnutChartOptions = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 85,
      tooltips: {
        enabled: false
      },
      hover: {
        animationDuration: 0 
      },
      legend: {
        position: 'left',
        labels: {
          //fontColor: "white",
          boxWidth: 7,
          padding: 5,
          usePointStyle: true
        }
      }
    };
    this.doughnutDataColors = [{ backgroundColor: ['#308ee0', '#1da1f2', '#e52d27'] }];

    // doughnut chart for Total Amount Invested
    var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
    this.doughnutChartData = [20, 80, 50];
    this.doughnutChartLabels = ['Facebook', 'Twitter', 'Youtube'];
    let _this = this;
    this.doughnutChartOptions.animation = {
      onComplete: function () {
        _this.centerLabel(ctx, "12.5k", "#2b2b2b", _this.doughnutCanvas);
      }
    }
  }

  /**
   * set doughnut chart for
   * Average Engagement Rate
   */
  setDounatTotalEngage() {
    this.doughnutChartOptions1 = JSON.parse(JSON.stringify(this.doughnutChartOptions));
    // doughnut chart for Total Amount Invested
    var ctx = this.doughnutCanvas1.nativeElement.getContext("2d");
    this.doughnutChartData1 = [20, 80, 50];
    let _this = this;
    this.doughnutChartOptions1.animation = {
      onComplete: function () {
        _this.centerLabel(ctx, "76%", "#2b2b2b", _this.doughnutCanvas1);
      }
    }
  }

   /**
   * set line chart for
   * Yotube Progress
   */
  setLineYotubeProgress(){
    // line chart labels
    this.lineChartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'];

    // line chart for followers reached
    this.lineChartData = [
      { data: [169, 226, 190, 223, 225], label: 'Youtube Progress' },
    ];
  }

   /**
   * set line chart data for
   * Instagram Progress
   */
  setLineInstagramProgress(){
    // line chart for followers reached
    this.lineChartData1 = [
      { data: [160, 225, 180, 220, 222], label: 'Instagram Progress' },
    ];
  }


  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx, middleText1, textColor1, doughnutCanvas) {
    var width = doughnutCanvas.nativeElement.clientWidth,
      height = doughnutCanvas.nativeElement.clientHeight;

    var fontSize = (height / 80).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "middle";
    ctx.fillStyle = textColor1;

    var text = middleText1,
      textX = Math.round((width - ctx.measureText(text).width) / 2),
      textY = height / 2;

    ctx.fillText(text, textX, textY, 200);
    ctx.restore();
  }




}
