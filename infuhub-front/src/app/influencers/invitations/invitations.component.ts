import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss']
})
export class InvitationsComponent implements OnInit {
  public social = {
    fbChecked : false,
    twChecked : false,
    uChecked : false,
    inChecked : false,
  };
  objectKeys = Object.keys;
  constructor() { }
  // public a: string[] = ['ff', 'sdf', 'sf']; 
  // public b:any[]=['sd', 3, true];

  ngOnInit() {
  }
  onOpenChange(t) {
    console.log(this.social)
  }

  objectLength(obj) {
    return Object.keys(obj);
  }


}
