import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
{
path: '',
loadChildren: './frontend/frontend.module#FrontendModule',
},
{
    path: 'brand-owner',
    loadChildren: './brand-owner/brand-owner.module#BrandOwnerModule'
},
{
    path: 'influencers',
    loadChildren: './influencers/influencers.module#InfluencersModule'
}
// { path: '', redirectTo: '/home', pathMatch: 'full'},
// { path: '**', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
exports: [RouterModule]
})
export class AppRoutingModule { }