import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Constant } from './constant';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgFlashMessagesModule } from 'ng-flash-messages';



@NgModule({
  declarations: [
    AppComponent,  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgFlashMessagesModule.forRoot(),

  ],
  providers: [
    Constant
       
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }