import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontendRoutingModule } from './frontend-routing.module';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  imports: [
    CommonModule,
    FrontendRoutingModule
  ],
  declarations: [ 
    HomeLayoutComponent, 
    HomeComponent, 
    HeaderComponent, 
    FooterComponent
  ]
})
export class FrontendModule { }
