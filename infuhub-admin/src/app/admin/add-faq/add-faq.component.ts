import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from '../services/common.service';
import { FaqService } from '../services/faq.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})

export class AddFaqComponent implements OnInit {

  public tinyMceSettings = {

    skin_url: '/assets/tinymce/skins/lightgray',
    inline: false,
    statusbar: false,
    browser_spellcheck: true,
    height: 120,
    plugins: 'fullscreen',
  };


  Form: FormGroup;
  submitted = false;

  id: number;
  quesn_en: any;
  ans_en: any;
  quesn_ar: any;
  ans_ar: any;
  status: string;
  action_name;

  constructor(private faqService: FaqService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.status = 'Active';
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });

   

    if (this.id > 0) {
      this.action_name='Edit';
      this.Form = new FormGroup({
        id: new FormControl(''),
        quesn_en: new FormControl('', <any>Validators.required),
        ans_en: new FormControl(''),
        quesn_ar: new FormControl(''),
        ans_ar: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });

    } else {
      this.action_name='Add';
      this.Form = new FormGroup({
        id: new FormControl(''),
        quesn_en: new FormControl('', <any>Validators.required),
        ans_en: new FormControl(''),
        quesn_ar: new FormControl(''),
        ans_ar: new FormControl(''),
        status: new FormControl('', <any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.Form.invalid) {
      return;
    } else {

      this.router.navigate(['/faq']);
    }
  }


  cancelForm = function () {
    this.router.navigate(['/faq']);
  }

}
