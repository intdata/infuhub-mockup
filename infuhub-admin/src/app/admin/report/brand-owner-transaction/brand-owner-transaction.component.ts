import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'app-brand-owner-transaction',
  templateUrl: './brand-owner-transaction.component.html',
  styleUrls: ['./brand-owner-transaction.component.scss']
})
export class BrandOwnerTransactionComponent implements OnInit, OnDestroy, AfterViewInit {
  transactionData: any[] = [];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor( private router: Router) { }
  ngOnInit() {

    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 50, 100],
      columnDefs: [
        {
          // Target the actions column
          targets: [5],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]

    };

    
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
    this.transactionData = JSON.parse(
      '[{ "id": 1, "job_title": "Advertisement", "payment_by": "Jhon", "payment_to": "Steave","amount": "150", "transaction_id": "JDM2345", "status": "Completed", "created_at": "14-03-2019" }, { "id": 2, "job_title": "Marketting", "payment_by": "Jaction", "payment_to": "Martin","amount": "125", "transaction_id": "JDM2770", "status": "Pending", "created_at": "15-02-2019" }]');
    });
   
    this.dtTrigger.next();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
              }
          });
      });
  });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


}
