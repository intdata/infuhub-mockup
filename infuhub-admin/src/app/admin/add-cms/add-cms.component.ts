import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { BrandOwnerService } from '../services/brand-owner.service';
import { CommonService } from '../services/common.service';
import { AdmincmsService } from '../services/admincms.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-add-cms',
  templateUrl: './add-cms.component.html',
  styleUrls: ['./add-cms.component.scss']
})
export class AddCmsComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
	title_en;
	description_en;
	shortdescription_en;
	title_ar;
	description_ar;
	shortdescription_ar;
	meta_title;
	meta_tag;
	meta_description;
	status ; 
  actionName;
  constructor(private admincmsService:AdmincmsService, private commonService:CommonService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}

  ngOnInit() {

    this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 

        
         
      if(this.id>0)
      {
        this.actionName = 'Edit';
        this.Form = new FormGroup({
        id: new FormControl(''),
        title_en: new FormControl('',<any>Validators.required),
        description_en: new FormControl(''),
        Short_desc_en:new FormControl(''),
        title_ar: new FormControl(''),
        description_ar: new FormControl(''),
        Short_desc_ar:new FormControl(''),
        meta_title: new FormControl(''),
        meta_tag: new FormControl(''),
        meta_description:new FormControl(''),
        status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.actionName = 'Edit';
      this.Form = new FormGroup({
        id: new FormControl(''),
        title_en: new FormControl('',<any>Validators.required),
        description_en: new FormControl(''),
        Short_desc_en:new FormControl(''),
        title_ar: new FormControl(''),
        description_ar: new FormControl(''),
        Short_desc_ar:new FormControl(''),
        meta_title: new FormControl(''),
        meta_tag: new FormControl(''),
        meta_description:new FormControl(''),
        status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {
          this.router.navigate(['/cms']);
        } 
    }


   cancelForm = function(){
    this.router.navigate(['/cms']);
  } 

}
