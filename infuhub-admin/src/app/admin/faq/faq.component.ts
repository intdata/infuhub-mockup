import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild} from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl} from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { FaqService } from '../services/faq.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})

export class FaqComponent implements OnInit ,OnDestroy,AfterViewInit {
   
  faqData :any[]=[];
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private faqService:FaqService, private router:Router) { }
   
  ngOnInit() {
    this.faqData = [{"id":2,"status":"Active","question":"Is this test question two?","answer":"<p>Yes, this is.</p>","lang_code":"en"},{"id":1,"status":"Active","question":"Is this test question one?","answer":"<p>Yes, this is.</p>","lang_code":"en"}]; 
    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [3],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
       
     this.dtTrigger.next();   
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  deleteFaq = function(id){
  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure to delete? ")) {
          this.faqService.deleteFaqData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

}

