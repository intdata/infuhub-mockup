import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { BrandOwnerService } from '../services/brand-owner.service';
import { DataTableDirective } from 'angular-datatables';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-brand-owner-list',
  templateUrl: './brand-owner-list.component.html',
  styleUrls: ['./brand-owner-list.component.scss']
})
export class BrandOwnerListComponent implements OnInit, OnDestroy, AfterViewInit {
  brandOwnerData: any[] = [];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private brandOwnerService: BrandOwnerService, private router: Router) { }
  ngOnInit() {

    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 50, 100],
      columnDefs: [
        {
          // Target the actions column
          targets: [0,8],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]

    };

    
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
    this.brandOwnerData = JSON.parse('[{ "id": 31 ,"fname": "Alex", "lname": "Morrison", "company_type" : "Agency","comapany_name": "Samsung Pvt Ltd", "industry": "Architecture", "country": "Australia","company_ph_no": 9831043166, "email": "nsuperadmin321@gmail.com", "logo_name": "bmw-300x300.jpg", "status": "Active", "created_at": "2019-02-28T10:01:12.000Z" },{ "id": 24, "fname": "othelo", "lname": "Vibgire","company_type" : "Brand","comapany_name": "Tcs Services Pvt Ltd",  "industry": "Architecture", "country": "Australia", "company_ph_no": 9831043160, "email": "tcs@gmail.com", "logo_name": "dell_logo_3529.gif", "status": "Active", "created_at": "2019-01-23T09:57:01.000Z" },{ "id": 29, "fname": "jhon", "lname": "Samurai","company_type" : "Agency","comapany_name": "cognizant Services Pvt Ltd",  "industry": "Market research", "country": "Germany", "company_ph_no": 9831043160, "email": "cts@gmail.com", "logo_name": "corporate-compnany.jpg", "status": "Active", "created_at": "2019-01-23T11:00:51.000Z" },{ "id": 6, "fname": "Avvyan", "lname": "Rhtherford","company_type" : "Brand","comapany_name": "Samsung Pvt Ltd",  "industry": "Architecture", "country": "Germany", "company_ph_no": 9831043167, "email": "Samsung.PvtLtd@samsung.com", "logo_name": "telecommunications.png", "status": "Active", "created_at": "2019-01-17T10:13:50.000Z" },{ "id": 28,  "fname": "Anjela", "lname": "Agusto","company_type" : "Agency","comapany_name": "indusnet Technology Pvt Ltd",  "industry": "Architecture", "country": "Germany", "company_ph_no": 9831043167, "email": "ins@gmail.com", "logo_name": "logo-designing.jpg", "status": "Inactive", "created_at": "2019-01-23T10:01:31.000Z" },{ "id": 23, "fname": "Stefen", "lname": "guchho","company_type" : "Brand","comapany_name": "Samsung Pvt Ltd",  "industry": "Architecture", "country": "Germany", "company_ph_no": 9831043167, "email": "rit@c.com", "logo_name": "logos-make-.png", "status": "Active", "created_at": "2019-01-22T11:14:44.000Z" }]');
    }
    
    );
   
    this.dtTrigger.next();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
              }
          });
      });
  });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteBrandOwnder = function (id) {

    if (confirm("Are you sure to delete Brand Owner? "))
     {
    }
  }

}
