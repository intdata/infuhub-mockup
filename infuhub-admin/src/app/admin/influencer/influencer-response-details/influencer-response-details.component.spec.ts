import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencerResponseDetailsComponent } from './influencer-response-details.component';

describe('InfluencerResponseDetailsComponent', () => {
  let component: InfluencerResponseDetailsComponent;
  let fixture: ComponentFixture<InfluencerResponseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencerResponseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencerResponseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
