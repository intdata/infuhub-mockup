import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-sitesettings',
  templateUrl: './sitesettings.component.html',
  styleUrls: ['./sitesettings.component.scss']
})
export class SitesettingsComponent implements OnInit {
  sitesetting;

  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sitesetting = [{ "id": 1, "setting_name": "Info Email", "setting_label": "info_email", "setting_value": "info@gmail.com", "created_at": null, "updated_at": null, "created_by": 0, "updated_by": 0 }, { "id": 2, "setting_name": "Contact Email", "setting_label": "contact_email", "setting_value": "contactus@gmail.com", "created_at": null, "updated_at": null, "created_by": 0, "updated_by": 0 }];

  }

}
