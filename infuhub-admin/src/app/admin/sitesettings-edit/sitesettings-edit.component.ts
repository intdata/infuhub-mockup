import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-sitesettings-edit',
  templateUrl: './sitesettings-edit.component.html',
  styleUrls: ['./sitesettings-edit.component.scss']
})
export class SitesettingsEditComponent implements OnInit {
  id;
  setting_name;
  setting_value;
  sitesettingForm;
  submitted = false;
  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.sitesettingForm = new FormGroup({
      id: new FormControl(),
      setting_name: new FormControl('', Validators.required),
      setting_value: new FormControl('', Validators.required)
    });

   
  }
  get f() { return this.sitesettingForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.sitesettingForm.invalid) {
      return;
    } else {

      this.router.navigate(['/sitesettings']);
     
    }
  }

  cancelForm = function () {
    this.router.navigate(['/sitesettings']);
  }


}
