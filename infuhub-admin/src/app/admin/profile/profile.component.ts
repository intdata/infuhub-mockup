import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../services/admin.service';
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  addadminForm: FormGroup;
  submitted = false;
  successMessage = "";
  id;
  fname;
  lname;
  email;
  status;
  image;

  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  filesToUpload: Array<File> = [];
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  ngOnInit() {
    this.fname = '';
        this.lname = '';
        this.email = '';
        this.status = '';
        this.image  = '';
    
    

    this.addadminForm = new FormGroup({
      fname: new FormControl('', <any>Validators.required),
      lname: new FormControl('', <any>Validators.required),
      email: new FormControl(''),
    });
  }
  get f() { return this.addadminForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.addadminForm.invalid) {
      return;
    } else {

      const files: Array<File> = this.filesToUpload;
      const profileData: any = new FormData();
      this.successMessage = "Profile updated successfully";
      
    }
  }
  cancelForm = function () {
    this.router.navigate(['/dashboard']);
  }

}
