
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';

import { AdminRoutingModule } from './admin-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { InnerLayoutComponent } from './inner-layout/inner-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { LoginComponent } from './login/login.component';
import { AddAdminComponent } from './add-admin/add-admin.component';
import { AdminService } from './services/admin.service';
import { AdminUserListComponent } from './admin-user-list/admin-user-list.component';
import { CreatorComponent } from './creator/creator.component';
import { AddCreatorComponent } from './add-creator/add-creator.component';
import { CreatorService } from './services/creator.service';
import { CreatorListComponent } from './creator-list/creator-list.component';
import { AddBrandOwnerComponent } from './add-brand-owner/add-brand-owner.component';
import { BrandOwnerListComponent } from './brand-owner-list/brand-owner-list.component';
import { BrandOwnerService } from './services/brand-owner.service';
import { DataTablesModule } from "angular-datatables";
import { CommonService } from './services/common.service';
import { CmsComponent } from './cms/cms.component';
import { AddCmsComponent } from "./add-cms/add-cms.component";
import { AdmincmsService } from './services/admincms.service';
import { PermissionComponent } from './permission/permission.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { MenuAddComponent } from './admin-menu/menu-add/menu-add.component';
import { NoPermissionComponent } from './no-permission/no-permission.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { FaqComponent } from './faq/faq.component';
import { ComparepasswordDirective } from './directive/comparepassword.directive';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { FaqService } from './services/faq.service';
import { StrStripHtmlPipe } from './pipe/str-strip-html.pipe';
import { SitesettingsEditComponent } from './sitesettings-edit/sitesettings-edit.component';

import { AdminRoleComponent } from './admin-role/admin-role.component';
import { AdminRolePermissionComponent } from './admin-role-permission/admin-role-permission.component';
import { AddeditAdminRoleComponent } from './addedit-admin-role/addedit-admin-role.component';
import { ProfileComponent } from './profile/profile.component';
//import { NgFlashMessagesModule } from 'ng-flash-messages';
import { FrontEndMenuManagementComponent } from './front-end-menu-management/front-end-menu-management.component';
import { AddMenuComponent } from './front-end-menu-management/add-menu/add-menu.component';
import { JobsComponent } from './brand-owner/jobs/jobs.component';
import { SentInvitationComponent } from './brand-owner/sent-invitation/sent-invitation.component';
import { ReceivedInvitationComponent } from './influencer/received-invitation/received-invitation.component';
import { InvitationDetailsComponent } from './influencer/invitation-details/invitation-details.component';
import { CampaignComponent } from './brand-owner/campaign/campaign.component';
import { CampaignDetailsComponent } from './brand-owner/campaign/campaign-details/campaign-details.component';
import { ResponseDetailComponent } from './brand-owner/response-detail/response-detail.component';
import { EmailTemplateComponent } from './cms/email-template/email-template.component';
import { EmailTemplateAddComponent } from './cms/email-template/email-template-add/email-template-add.component';
import { InfluencerResponseDetailsComponent } from './influencer/influencer-response-details/influencer-response-details.component';
import { InfluencerTransactionComponent } from './report/influencer-transaction/influencer-transaction.component';
import { BrandOwnerTransactionComponent } from './report/brand-owner-transaction/brand-owner-transaction.component';
import { TestimonialComponent } from './cms/testimonial/testimonial.component';
import { AddTestimonialComponent } from './cms/testimonial/add-testimonial/add-testimonial.component';
import { CampaignAddEditComponent } from './brand-owner/campaign/campaign-add-edit/campaign-add-edit.component';
import { InfluencerDetailsComponent } from './influencer/influencer-details/influencer-details.component';



//import { NgFlashMessagesModule } from 'ng-flash-messages';

@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    UserComponent,
    InnerLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    AddAdminComponent,
    AdminUserListComponent,
    CreatorComponent,
    AddCreatorComponent,
    CreatorListComponent,
    AddBrandOwnerComponent,
    BrandOwnerListComponent,
    CmsComponent,
    AddCmsComponent,
    PermissionComponent,
    AdminMenuComponent,
    MenuAddComponent,
    NoPermissionComponent,
    ChangePasswordComponent,
    SitesettingsComponent,
    FaqComponent,
    ComparepasswordDirective,
    AddFaqComponent,
    StrStripHtmlPipe,
    SitesettingsEditComponent,
    AdminRoleComponent,
    AdminRolePermissionComponent,
    AddeditAdminRoleComponent,
    ProfileComponent,
    FrontEndMenuManagementComponent,
    AddMenuComponent,
    JobsComponent,
    SentInvitationComponent,
    ReceivedInvitationComponent,
    InvitationDetailsComponent,
    CampaignComponent,
    CampaignDetailsComponent,
    ResponseDetailComponent,
    EmailTemplateComponent,
    EmailTemplateAddComponent,
    InfluencerResponseDetailsComponent,
    InfluencerTransactionComponent,
    BrandOwnerTransactionComponent,
    TestimonialComponent,
    AddTestimonialComponent,
    CampaignAddEditComponent,
    InfluencerDetailsComponent
  
  ],
  imports: [
    CommonModule,
    RouterModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,    
    DataTablesModule,
    EditorModule,

    NgbModule.forRoot(),
   // NgFlashMessagesModule.forRoot(),
    NgxPermissionsModule.forChild()
    
  ],
  providers: [
    AdminService,    
    CreatorService,
    BrandOwnerService,
    CommonService,
    AdmincmsService,
    FaqService,
  ]
})
export class AdminModule { }
