import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { AdmincmsService } from '../../services/admincms.service';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})

export class EmailTemplateComponent implements OnInit ,OnDestroy,AfterViewInit {
   
   cmsData :any[]=[];
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private admincmsService:AdmincmsService, private router:Router) { }
   
  ngOnInit() {
    this.cmsData = [{"id":41,"status":"Active","title":"Forget Password Link Password Confirmation","subject":"Account - Password Changed"},{"id":40,"status":"Active","title":"Forget Password Link","subject":"Account - Password Reset Link"},{"id":39,"status":"Active","title":"Payment Notification","subject":"Payment made successfully"}];
    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [3],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
       
    
    this.dtTrigger.next();
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  deleteCms = function(id){

  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure to delete? ")) {
          this.admincmsService.deleteCmsData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

}

