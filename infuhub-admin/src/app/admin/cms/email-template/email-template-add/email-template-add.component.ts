import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { BrandOwnerService } from '../../../services/brand-owner.service';
import { CommonService } from '../../../services/common.service';
import { AdmincmsService } from '../../../services/admincms.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-email-template-add',
  templateUrl: './email-template-add.component.html',
  styleUrls: ['./email-template-add.component.scss']
})
export class EmailTemplateAddComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
	template_name;
	description;
	subject;
	from_name;
	from_email;

	status ; 
  actionName;
  constructor(private admincmsService:AdmincmsService, private commonService:CommonService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}

  ngOnInit() {

    this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 

        
         
      if(this.id>0)
      {
        this.actionName = 'Edit';
        this.Form = new FormGroup({
        id: new FormControl(''),
        template_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        subject:new FormControl('',<any>Validators.required),
        from_name: new FormControl('',<any>Validators.required),
        from_email: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.actionName = 'Add';
      this.Form = new FormGroup({
        id: new FormControl(''),
        template_name: new FormControl('',<any>Validators.required),
        
        description: new FormControl('',<any>Validators.required),
        subject:new FormControl('',<any>Validators.required),
        from_name: new FormControl('',<any>Validators.required),
        from_email: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {
          this.router.navigate(['/email-template']);
        } 
    }


   cancelForm = function(){
    this.router.navigate(['/email-template']);
  } 

}

