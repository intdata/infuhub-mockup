
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { BrandOwnerService } from '../../../services/brand-owner.service';
import { CommonService } from '../../../services/common.service';
import { AdmincmsService } from '../../../services/admincms.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-add-testimonial',
  templateUrl: './add-testimonial.component.html',
  styleUrls: ['./add-testimonial.component.scss']
})
export class AddTestimonialComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
  author_name;
  author_img;
	description;
	designation;


	status ; 
  actionName;
  constructor(private admincmsService:AdmincmsService, private commonService:CommonService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}

  ngOnInit() {

    this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 

        
         
      if(this.id>0)
      {
        this.actionName = 'Edit';
        this.Form = new FormGroup({
        id: new FormControl(''),
        author_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        author_img: new FormControl('',<any>Validators.required),
        designation: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.actionName = 'Add';
      this.Form = new FormGroup({
        id: new FormControl(''),
        author_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        author_img: new FormControl('',<any>Validators.required),
        designation: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {
          this.router.navigate(['/testimonial']);
        } 
    }


   cancelForm = function(){
    this.router.navigate(['/testimonial']);
  } 

}


