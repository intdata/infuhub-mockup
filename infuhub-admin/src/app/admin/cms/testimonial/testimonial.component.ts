
import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { AdmincmsService } from '../../services/admincms.service';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})

export class TestimonialComponent implements OnInit ,OnDestroy,AfterViewInit {
   
   cmsData :any[]=[];
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private admincmsService:AdmincmsService, private router:Router) { }
   
  ngOnInit() {
    this.cmsData = [{"id":41,"status":"Active","title":"Mark Heans","subject":"Lorem Ipsum is simply dummy text of the printing the printing m Ipsum Ipsum is simply.... ","profile_img":"face1.jpg","designation":"Co-Founder"},{"id":40,"status":"Active","title":"Rik Salvaton","subject":"Lorem Ipsum is simply dummy text of the printing and typesetting industry","profile_img":"face2.jpg","designation":"Director"},{"id":39,"status":"Active","title":"stafen Agusto","subject":"Lorem Ipsum is simply dummy text of the printing and typesetting industry.","profile_img":"face3.jpg","designation":"MD & CEO"}];
    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [3],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
       
    
    this.dtTrigger.next();
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  deleteCms = function(id){

  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure to delete? ")) {
          this.admincmsService.deleteCmsData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

}


