import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AdminService } from '../services/admin.service';
@Component({
    selector: 'app-admin-user-list',
    templateUrl: './admin-user-list.component.html',
    styleUrls: ['./admin-user-list.component.scss']
})
export class AdminUserListComponent implements OnInit, OnDestroy, AfterViewInit {
    adminUserData: any[] = [];
    adminId;
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: any = {};
    dtTrigger: Subject<any> = new Subject();
    constructor(private adminService: AdminService, private router: Router) { }
    ngOnInit() {
        this.adminUserData = [{"admin_type":"sub-admin","id":86,"email":"utpalpoulik@yahoo.com","image":"","first_name":"Utpal","last_name":"Poulik","status":"Active","created_at":"2019-02-05T12:10:29.000Z"},{"admin_type":"sub-admin","id":87,"email":"asesh@gmail.com","image":"","first_name":"Asesh","last_name":"Pal","status":"Active","created_at":"2019-02-05T12:13:37.000Z"},{"admin_type":"sub-admin","id":88,"email":"sumonn@admin.com","image":"","first_name":"sumon","last_name":"Chowdhory","status":"Active","created_at":"2019-02-05T12:21:36.000Z"},{"admin_type":"sub-admin","id":89,"email":"test@admin.com","image":"","first_name":"Test Sub Admin","last_name":"Admin","status":"Active","created_at":"2019-02-07T06:26:42.000Z"},{"admin_type":"sub-admin","id":90,"email":"utpalchow@admin.com","image":"","first_name":"utpal","last_name":"Chowdhory","status":"Active","created_at":"2019-02-07T06:34:04.000Z"},{"admin_type":"sub-admin","id":91,"email":"subadmin@gmail.com","image":"","first_name":"Sub Admin","last_name":"Test","status":"Active","created_at":"2019-02-07T06:53:02.000Z"},{"admin_type":"creator","id":93,"email":"creator@gmail.com","image":"","first_name":"New Creator","last_name":"Test","status":"Active","created_at":"2019-02-07T13:33:46.000Z"},{"admin_type":"sub-admin","id":95,"email":"kk@gmail.com","image":"","first_name":"utpal123","last_name":"poulik","status":"Active","created_at":"2019-03-13T06:19:52.000Z"}];
        this.dtOptions = {
            //pagingType: 'full_numbers',
            pageLength: 10,
            columnDefs: [
                {
                    // Target the actions column
                    targets: [4],
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                    orderable: false
                }
            ]
            //processing: true
        };
    }
    ngAfterViewInit(): void {
 
        
        this.dtTrigger.next();

        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
                const that = this;
                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this['value']) {
                        that.search(this['value']).draw();
                    }
                });
            });
        });
    }
    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }
    deleteAdmin = function (id) {           
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            if (confirm("Are you sure to delete? ")) {
                this.adminService.deleteAdminData(id).subscribe(data => {
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        dtInstance.destroy();
                        this.ngAfterViewInit();
                    }
                });
            }
        });
    }
}
