import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  passwordFormGroup;
  submitted = false;
  serverMessage;
  serverResponceCode;
 
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {}

  ngOnInit() {

	  	this.changePasswordForm = new FormGroup({
		    old_password 	: new FormControl('', < any > Validators.required),
		    new_password 	: new FormControl('', < any > Validators.required),
		    confirm_password: new FormControl('', < any > Validators.required)
		});
  }
	get f() {
        return this.changePasswordForm.controls;
    }
  onSubmit() {
        this.submitted = true;
        if(this.changePasswordForm.invalid) {
            return;
        } else {
          this.serverResponceCode = 1;
          this.serverMessage = "Password changed successfully";
          //this.router.navigate(['/admin/dashboard']);
        		
            }
    }

    cancelForm = function() {
        this.router.navigate(['/dashboard']);
    }
}
