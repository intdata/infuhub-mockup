import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, AfterViewInit {
  menuData = {};
  admin_name;
  //variable = [];

  constructor() {
    //this.variable['value'] = true;  
  }
  ngOnInit() {
    this.admin_name = 'Alex Simphony';
    
      this.menuData = { "1": { "parentMenuName": "Users", "menuList": [{ "id": 2, "menu_name": "Influencer", "menu_slug": "influencer" }, { "id": 3, "menu_name": "Brand Owner", "menu_slug": "brand-owner" }, { "id": 4, "menu_name": "Admin User", "menu_slug": "admin-user" }] }, "2": { "parentMenuName": "Content Management", "menuList": [{ "id": 6, "menu_name": "Pages", "menu_slug": "cms" }, { "id": 8, "menu_name": "Help Center", "menu_slug": "faq" },{ "id": 9, "menu_name": "Email Template", "menu_slug": "email-template" },{ "id": 9, "menu_name": "Testimonial", "menu_slug": "testimonial" }] }, "3": { "parentMenuName": "Campaign", "menuList": [{ "id": 1, "menu_name": "Campaign List", "menu_slug": "campaign-list" }] }, "4": { "parentMenuName": "Settings", "menuList": [{ "id": 10, "menu_name": "Front Menu Management", "menu_slug": "front-menu" }, { "id": 12, "menu_name": "Admin Role Management", "menu_slug": "admin-role" }, { "id": 11, "menu_name": "Site Settings", "menu_slug": "sitesettings" }] }, "5": { "parentMenuName": "Report", "menuList": [{ "id": 15, "menu_name": "Influencer Transaction", "menu_slug": "influencer-transaction" }, { "id": 16, "menu_name": "Brand Owner Transaction", "menu_slug": "brand-owner-transaction" }] } };
   
  }

  ngAfterViewInit(): void {




  }


}
