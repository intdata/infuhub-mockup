import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
@Injectable({
  providedIn: 'root'
})
export class AdmincmsService {

  constructor(private http: Http, private constant:Constant) { }



	addeditCmsData(cmsData){
	  	return this.http.post(this.constant.BASE_URL+'admincms/addeditCmsData/',cmsData).map((response: Response) =>response.json());  
	}

	
	deleteCmsData(id){
		
	  	return this.http.get(this.constant.BASE_URL+'admincms/deleteCmsData/'+id).map((response: Response) =>response.json());  
	}


	getAllCmsData(){


	  	return this.http.get(this.constant.BASE_URL+'admincms/getAllCmsList/').map((response: Response) =>response.json());  
	}

    getSingleCmsData(id){

	  	return this.http.get(this.constant.BASE_URL+'admincms/getEditData/'+id).map((response: Response) =>response.json());  
	   
	} 
}


