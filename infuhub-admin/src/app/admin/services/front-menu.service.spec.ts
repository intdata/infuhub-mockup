import { TestBed } from '@angular/core/testing';

import { FrontMenuService } from './front-menu.service';

describe('FrontMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrontMenuService = TestBed.get(FrontMenuService);
    expect(service).toBeTruthy();
  });
});
