import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Constant } from '../../constant';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
	providedIn: 'root'
})
export class AdminMenuService {

	constructor(private http: Http, private constant: Constant) { }

	addMenuData(data) {
		return this.http.post(this.constant.BASE_URL + 'adminmenu/addMenu/', data).map((response: Response) => response.json());
	}

	getMenuData() {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/menuList/').map((response: Response) => response.json());
	}

	getMenuGroupData() {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/menuGroupName/').map((response: Response) => response.json());
	}

	getSingleMenuData(id) {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/singleMenu/' + id).map((response: Response) => response.json());
	}

	getMenuPermissionDataByAdminId(adminId) {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/menuPermissionList/' + adminId).map((response: Response) => response.json());
	}


	permission(data) {
		return this.http.post(this.constant.BASE_URL + 'adminmenu/permission/', data).map((response: Response) => response.json());
	}

	getPermission(data) {
		return this.http.post(this.constant.BASE_URL + 'adminmenu/getPermission/', data).map((response: Response) => response.json());
	}

	getSidebarMenuData() {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/sidebarMenu/').map((response: Response) => response.json());
	}
	//===============Role Permission======================//
	getMenuPermissionDataByRoleId(roleId) {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/menuRolePermissionList/' + roleId).map((response: Response) => response.json());
	}
	getAdminRoleTypeName(roleId) {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/getAdminRoleTypeName/' + roleId).map((response: Response) => response.json());
	}
	rolePermission(data) {
		return this.http.post(this.constant.BASE_URL + 'adminmenu/rolePermission/', data).map((response: Response) => response.json());
	}

	getParentMenuName(id){
		return this.http.get(this.constant.BASE_URL+'adminmenu/parentMenuName/'+id).map((response: Response) =>response.json());	
	}
}
