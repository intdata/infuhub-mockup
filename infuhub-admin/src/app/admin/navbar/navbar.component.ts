import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {Constant} from '../../constant';

import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {
  public sidebarOpened = false;
  admin_name: string;
  toggleOffcanvas() {
    this.sidebarOpened = !this.sidebarOpened;
    if (this.sidebarOpened) {
      document.querySelector('.sidebar-offcanvas').classList.add('active');
    }
    else {
      document.querySelector('.sidebar-offcanvas').classList.remove('active');
    }
  }
  constructor(config: NgbDropdownConfig, private constant:Constant, private adminService: AdminService, private router:Router) {
    config.placement = 'bottom-right';
  }

  ngOnInit() {

    this.admin_name ='Alex Simphony';
   
  }

  adminLogout(){
  
    this.router.navigate([this.constant.ADMIN_PATH+'login']);
  }

}
