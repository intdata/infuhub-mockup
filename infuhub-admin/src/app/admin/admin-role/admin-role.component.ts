import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-admin-role',
  templateUrl: './admin-role.component.html',
  styleUrls: ['./admin-role.component.scss']
})
export class AdminRoleComponent implements OnInit, OnDestroy, AfterViewInit {
  adminRoleData: any[] = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  constructor(private adminService: AdminService, private router: Router) { }
  ngOnInit() {
    this.adminRoleData =  [{"id":6,"role_name":"Creator","admin_type":"creator","status":"Active","created_at":"2019-02-07T10:11:29.000Z","updated_at":"2019-02-07T10:11:29.000Z","created_by":0,"updated_by":0},{"id":5,"role_name":"Sub Admin","admin_type":"sub-admin","status":"Active","created_at":"2019-02-07T09:50:20.000Z","updated_at":"2019-02-07T09:50:20.000Z","created_by":0,"updated_by":0},{"id":1,"role_name":"Super Admin","admin_type":"super-admin","status":"Active","created_at":"2019-02-05T12:05:55.000Z","updated_at":"2019-02-05T18:30:00.000Z","created_by":0,"updated_by":0}];
    
    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      columnDefs: [
        {
          // Target the actions column
          targets: [2],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]
      //processing: true
    };


  }


  ngAfterViewInit(): void {
    
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  deleteAdmin = function (id) {
  
      if (confirm("Are you sure to delete? ")) {
      }
  
  }
}
