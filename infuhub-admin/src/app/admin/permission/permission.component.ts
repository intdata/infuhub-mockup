import { Component, OnInit } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { AdminMenuService } from '../services/admin-menu.service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  constructor(private adminMenuService:AdminMenuService, private activatedRoute: ActivatedRoute) { }
  data = {};
  permissionData = [];
  menuPermissionData;
  adminId;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
            this.adminId = params['id'];
        });
     
        this.menuPermissionData = [{"id":55,"admin_menu_id":2,"admin_user_id":87,"permission_view":0,"permission_add":0,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Influencer","menu_slug":"creator"},{"id":56,"admin_menu_id":3,"admin_user_id":87,"permission_view":0,"permission_add":0,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Brand Owner","menu_slug":"brand-owner"},{"id":57,"admin_menu_id":4,"admin_user_id":87,"permission_view":0,"permission_add":0,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Admin User","menu_slug":"admin-user"},{"id":59,"admin_menu_id":6,"admin_user_id":87,"permission_view":1,"permission_add":1,"permission_edit":1,"permission_delete":1,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"CMS List","menu_slug":"cms"},{"id":61,"admin_menu_id":8,"admin_user_id":87,"permission_view":1,"permission_add":1,"permission_edit":1,"permission_delete":1,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Faq List","menu_slug":"faq"},{"id":99,"admin_menu_id":12,"admin_user_id":87,"permission_view":0,"permission_add":0,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":" Role Management","menu_slug":"admin-role"}]
    
  }

  permission(menu_id,action,event){
  	this.data['admin_id']      = this.adminId;
  	this.data['menu_id']       = menu_id;
    this.data['permission_action']  = action;
    if(event.target.checked == true){
      this.data['is_check'] = 1;
    }
    else{
      this.data['is_check'] = 0;
    }
   
  	this.adminMenuService.permission(this.data).subscribe(data=>{});  	
   
  }

}
