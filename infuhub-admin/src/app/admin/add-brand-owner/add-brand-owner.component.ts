import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { BrandOwnerService } from '../services/brand-owner.service';
import { CommonService } from '../services/common.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-add-brand-owner',
  templateUrl: './add-brand-owner.component.html',
  styleUrls: ['./add-brand-owner.component.scss']
})
export class AddBrandOwnerComponent implements OnInit {

  brandOwnerForm: FormGroup;
  submitted = false;
  isPassword = true;
  isEmailExit = false;
  brandEmailData={};

  id;
  comapany_name;
  company_ph_no;
  email;
  company_logo;
  company_type;
  status;
  industry;
  country;
  fname;
  lname;

  action_name;

  debouncer: any;

  constructor(private brandOwnerService: BrandOwnerService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {


  }

  ngOnInit() {

      this.status = 'Active';
      this.industry="";
      this.country="";
      this.company_type="";

      this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });


    if (this.id > 0) {

      this.brandOwnerService.getSingleBrandOwnerData(this.id).subscribe(data => {

        if (data.msgcode == 1 && data.results.length > 0) {

          this.id = data.results[0].id;
          this.comapany_name = data.results[0].comapany_name;
          this.company_ph_no = data.results[0].company_ph_no;
          this.email = data.results[0].email;
          this.status = data.results[0].status;
        }

      });

    }

    if (this.id > 0) {

      this.action_name='Edit';
      this.isPassword = false;
      this.brandOwnerForm = new FormGroup({
        id: new FormControl(''),
        comapany_name: new FormControl('', <any>Validators.required),
        company_logo: new FormControl(''),
        company_type: new FormControl(''),
        industry: new FormControl('', <any>Validators.required),
        country: new FormControl('', <any>Validators.required),
        company_ph_no: new FormControl('', <any>[Validators.required, Validators.pattern("^[0-9]*$"),
        Validators.minLength(10), Validators.maxLength(10)]),
        fname: new FormControl('', <any>Validators.required),
        lname: new FormControl('', <any>Validators.required),
        password: new FormControl(''),
        email: new FormControl('', <any>[Validators.required, Validators.email]),
        status: new FormControl('', <any>Validators.required)
      });

    } else {
      
      this.action_name='Add';
      this.isPassword = true;
      this.brandOwnerForm = new FormGroup({

        id: new FormControl(''),
        comapany_name: new FormControl('', <any>Validators.required),
        company_logo: new FormControl('', <any>Validators.required),
        company_type: new FormControl('', <any>Validators.required),
        industry: new FormControl('', <any>Validators.required),
        country: new FormControl('', <any>Validators.required),
        company_ph_no: new FormControl('', <any>[Validators.required, Validators.pattern("^[0-9]*$"),
        Validators.minLength(10), Validators.maxLength(10)]),
        fname: new FormControl('', <any>Validators.required),
        lname: new FormControl('', <any>Validators.required),
        password: new FormControl('', <any>Validators.required),
        email: new FormControl('', <any>[Validators.required, Validators.email]),
        status: new FormControl('', <any>Validators.required)
      });


    }
  }

  get f() { return this.brandOwnerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.brandOwnerForm.invalid) {
      return;
    } else {

      this.router.navigate(['/brand-owner']);

    }
  }


  cancelForm = function () {
    this.router.navigate(['/brand-owner']);
  }

 

}
