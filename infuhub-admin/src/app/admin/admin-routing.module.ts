import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { InnerLayoutComponent } from './inner-layout/inner-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
//import { UserComponent } from './user/user.component';
import { AddAdminComponent } from './add-admin/add-admin.component';
import { AdminAuthGuard } from '../auth/admin-auth.guard';
import { AdminUserListComponent } from './admin-user-list/admin-user-list.component';
import { CreatorComponent } from './creator/creator.component';
import { AddCreatorComponent } from './add-creator/add-creator.component';
import { CreatorListComponent } from './creator-list/creator-list.component';
import { AddBrandOwnerComponent } from './add-brand-owner/add-brand-owner.component';
import { BrandOwnerListComponent } from './brand-owner-list/brand-owner-list.component';
import { CmsComponent } from './cms/cms.component';
import { AddCmsComponent } from './add-cms/add-cms.component';
import { PermissionComponent } from './permission/permission.component';
import { MenuAddComponent } from './admin-menu/menu-add/menu-add.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { NoPermissionComponent } from './no-permission/no-permission.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { AddFaqComponent } from './add-faq/add-faq.component'
import { FaqComponent } from './faq/faq.component';
import { SitesettingsEditComponent } from './sitesettings-edit/sitesettings-edit.component';
import { AdminRoleComponent } from './admin-role/admin-role.component';
import { AdminRolePermissionComponent } from './admin-role-permission/admin-role-permission.component';
import { AddeditAdminRoleComponent } from './addedit-admin-role/addedit-admin-role.component';
import { ProfileComponent } from './profile/profile.component';
import { FrontEndMenuManagementComponent } from './front-end-menu-management/front-end-menu-management.component';
import { AddMenuComponent } from './front-end-menu-management/add-menu/add-menu.component';
import { JobsComponent } from './brand-owner/jobs/jobs.component';
import { SentInvitationComponent } from './brand-owner/sent-invitation/sent-invitation.component';
import { ReceivedInvitationComponent } from './influencer/received-invitation/received-invitation.component';
import { InvitationDetailsComponent } from './influencer/invitation-details/invitation-details.component';
import { CampaignComponent } from './brand-owner/campaign/campaign.component';
import { CampaignDetailsComponent } from './brand-owner/campaign/campaign-details/campaign-details.component';
import { ResponseDetailComponent } from './brand-owner/response-detail/response-detail.component';
import { EmailTemplateComponent } from './cms/email-template/email-template.component';
import { InfluencerResponseDetailsComponent } from './influencer/influencer-response-details/influencer-response-details.component';
import { InfluencerTransactionComponent } from './report/influencer-transaction/influencer-transaction.component';
import { EmailTemplateAddComponent } from './cms/email-template/email-template-add/email-template-add.component';
import { BrandOwnerTransactionComponent } from './report/brand-owner-transaction/brand-owner-transaction.component';
import { AddTestimonialComponent } from './cms/testimonial/add-testimonial/add-testimonial.component';
import { TestimonialComponent } from './cms/testimonial/testimonial.component';
import { CampaignAddEditComponent } from './brand-owner/campaign/campaign-add-edit/campaign-add-edit.component';
import { InfluencerDetailsComponent } from './influencer/influencer-details/influencer-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: '', component: LoginLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent }
    ]
  },
  {
    path: '', component: InnerLayoutComponent,
    children: [
      { path: 'no-permission', component: NoPermissionComponent },
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'add-admin', component: AddAdminComponent
      },

      {
        path: 'editadmin/:id', component: AddAdminComponent,


      },
      {
        path: 'admin-user', component: AdminUserListComponent,


      },
      {
        path: 'add-influencer', component: AddCreatorComponent,
      },
      {
        path: 'influencer', component: CreatorListComponent,
      },
      {
        path: 'edit-influencer/:id', component: AddCreatorComponent,

      },
      {
        path: 'add-brand-owner', component: AddBrandOwnerComponent,
      },
      {
        path: 'editbrand-owner/:id', component: AddBrandOwnerComponent,
      },
      {
        path: 'brand-owner', component: BrandOwnerListComponent,
      },
      {
        path: 'jobs/:id', component: JobsComponent,
      },
      {
        path: 'invitation/:id', component: SentInvitationComponent,
      },
      {
        path: 'response-details/:id', component: ResponseDetailComponent,
      },
      
      {
        path: 'campaign-list/:id', component: CampaignComponent,
      },
      {
        path: 'campaign-list', component: CampaignComponent,
      },
      {
        path: 'campaign-details/:id', component: CampaignDetailsComponent,
      },
      {
        path: 'add-campaign', component: CampaignAddEditComponent,
      },
      {
        path: 'edit-campaign/:id', component: CampaignAddEditComponent,
      },
      {
        path: 'menu-add', component: MenuAddComponent,

      },
      {
        path: 'menu', component: AdminMenuComponent,
      },
      {
        path: 'menu-edit/:id', component: MenuAddComponent,
      },
      {
        path: 'cms', component: CmsComponent,
      },
      {
        path: 'add-cms', component: AddCmsComponent,
      },
      {
        path: 'email-template', component: EmailTemplateComponent,
      },
      {
        path: 'add-email-template', component: EmailTemplateAddComponent,
      },

      {
        path: 'edit-email-template/:id', component: EmailTemplateAddComponent,
      },
      
      {
        path: 'edit-cms/:id', component: AddCmsComponent,

      },
      {
        path: 'faq', component: FaqComponent,
      },
      {
        path: 'add-faq', component: AddFaqComponent,
      },
      {
        path: 'edit-faq/:id', component: AddFaqComponent,
      },
      { path: 'change-password', component: ChangePasswordComponent },
      {
        path: 'sitesettings', component: SitesettingsComponent,
      },
      {
        path: 'sitesettings-edit/:id', component: SitesettingsEditComponent,
      },
      { path: 'admin-role', component: AdminRoleComponent },
      { path: 'add-admin-role', component: AddeditAdminRoleComponent },
      { path: 'admin-role-permission/:id', component: AdminRolePermissionComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'front-menu', component: FrontEndMenuManagementComponent },
      { path: 'add-front-menu', component: AddMenuComponent },
      { path: 'edit-front-menu/:id', component: AddMenuComponent },
      { path: 'permission/:id', component: PermissionComponent },
      { path: 'received-invitation/:id', component: ReceivedInvitationComponent },
      { path: 'invitation-details/:id', component: InvitationDetailsComponent },
      { path: 'influencer-response-details/:id', component: InfluencerResponseDetailsComponent },
      { path: 'influencer-transaction', component: InfluencerTransactionComponent },
      { path: 'brand-owner-transaction', component: BrandOwnerTransactionComponent },
      { path: 'testimonial', component: TestimonialComponent },
      { path: 'add-testimonial', component: AddTestimonialComponent },
      { path: 'edit-testimonial/:id', component: AddTestimonialComponent },
      { path: 'influencer-details/:id', component: InfluencerDetailsComponent },
      

    ]
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AdminRoutingModule { }
