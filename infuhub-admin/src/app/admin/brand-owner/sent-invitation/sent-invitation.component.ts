import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-sent-invitation',
  templateUrl: './sent-invitation.component.html',
  styleUrls: ['./sent-invitation.component.scss']
})
export class SentInvitationComponent implements OnInit, OnDestroy, AfterViewInit {
  brandOwnerData: any[] = [];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor( private router: Router) { }
  ngOnInit() {

    console.log('All invitatation');
    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 50, 100],
      columnDefs: [
        {
          // Target the actions column
         // targets: [5],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]

    };

    
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
    this.brandOwnerData = JSON.parse(
      '[{ "id": 31, "influencer_name": "Rahul Dev", "job_title": "Image Creation For Social Network", "invitation_status": "Pending","invitation_date": "04-03-2019","created_at": "2019-02-28T10:01:12.000Z" },{ "id": 32, "influencer_name": "Sukanta Dutta", "job_title": "video Creation For Social Network", "invitation_status": "Accepted","invitation_date": "05-03-2019","created_at": "2019-02-28T10:01:12.000Z" },{ "id": 33, "influencer_name": "Jhon collin", "job_title": "Advertisement", "invitation_status": "Pending","invitation_date": "08-03-2019","created_at": "2019-02-28T10:01:12.000Z" }]');
    });
   
    this.dtTrigger.next();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
              }
          });
      });
  });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteBrandOwnder = function (id) {

    if (confirm("Are you sure to delete Brand Owner? "))
     {
    }
  }

}


