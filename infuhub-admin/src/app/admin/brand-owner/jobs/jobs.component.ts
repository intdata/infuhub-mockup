
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit, OnDestroy, AfterViewInit {
  brandOwnerData: any[] = [];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor( private router: Router) { }
  ngOnInit() {

    this.dtOptions = {
      ////pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 50, 100],
      columnDefs: [
        {
          // Target the actions column
          targets: [5],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]

    };

    
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
    this.brandOwnerData = JSON.parse(
      '[{ "id": 31, "comapany_name": "Website Build", "industry": "Architecture", "country": "Facebook","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Progress", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "comapany_name": "Advertisement", "industry": "Advertisement", "country": "Youtube","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Approval", "created_at": "2019-02-28T10:01:12.000Z" },{ "id": 31, "comapany_name": "Image Creation For Social Network", "industry": "Network", "country": "Youtube","company_ph_no": "04-03-2019", "email": "14-03-2019", "status": "Approved", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "comapany_name": "Video Creation For Youtube", "industry": "Youtube", "country": "Youtube","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Published", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "comapany_name": "Website Development", "industry": "Architecture", "country": "Facebook","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Paid", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "comapany_name": "Data Manupulation", "industry": "Network", "country": "Twitter","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Progress", "created_at": "2019-02-28T10:01:12.000Z" }]');
    });
   
    this.dtTrigger.next();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
              }
          });
      });
  });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteBrandOwnder = function (id) {

    if (confirm("Are you sure to delete Brand Owner? "))
     {
    }
  }

}


