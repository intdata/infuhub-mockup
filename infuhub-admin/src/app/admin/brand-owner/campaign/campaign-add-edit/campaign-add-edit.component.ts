
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { BrandOwnerService } from '../../../services/brand-owner.service';
import { CommonService } from '../../../services/common.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-campaign-add-edit',
  templateUrl: './campaign-add-edit.component.html',
  styleUrls: ['./campaign-add-edit.component.scss']
})
export class CampaignAddEditComponent implements OnInit {

  brandOwnerForm: FormGroup;
  submitted = false;
 
   brandEmailData={};

  id;
  brand_owner_name="";
  campaign_name;
  social_media;
  campaign_details;
  campaign_post_date;
  campaign_start_date;
  campaign_end_date;
  total_investment;
  roi_of_campaign;
  status;
  action_name;
  campaign_details_name;

  debouncer: any;

  constructor(private brandOwnerService: BrandOwnerService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {


  }

  ngOnInit() {

      this.status = 'Active';
     

      this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });


  

    if (this.id > 0) {

      this.action_name='Edit';
    

      this.brandOwnerForm = new FormGroup({
        id: new FormControl(''),
        
        brand_owner_name: new FormControl('', <any>Validators.required),
        campaign_name: new FormControl('', <any>Validators.required),
        campaign_details: new FormControl('', <any>Validators.required),
        campaign_post_date: new FormControl('', <any>Validators.required),
        campaign_start_date: new FormControl('', <any>Validators.required),
        campaign_end_date: new FormControl('', <any>Validators.required),
        total_investment: new FormControl('', <any>Validators.required),
        roi_of_campaign: new FormControl('', <any>Validators.required),
        social_media: new FormControl('',),
        no_of_tag: new FormControl('',),
        num_follwer: new FormControl('',),
        no_like_comnt: new FormControl('',),
        populer_region: new FormControl('', <any>Validators.required),
        potential_impressions: new FormControl('',),
        status: new FormControl('', <any>Validators.required)
      });

    } else {
      
      this.action_name='Add';
  
      this.brandOwnerForm = new FormGroup({

        id: new FormControl(''),
        brand_owner_name: new FormControl('', <any>Validators.required),
        campaign_name: new FormControl('', <any>Validators.required),
        campaign_details: new FormControl('', <any>Validators.required),
        campaign_post_date: new FormControl('', <any>Validators.required),
        campaign_start_date: new FormControl('', <any>Validators.required),
        campaign_end_date: new FormControl('', <any>Validators.required),
        total_investment: new FormControl('', <any>Validators.required),
        roi_of_campaign: new FormControl('', <any>Validators.required),
        social_media: new FormControl('',),
        no_of_tag: new FormControl('',),
        num_follwer: new FormControl('',),
        no_like_comnt: new FormControl('',),
        populer_region: new FormControl('', <any>Validators.required),
        potential_impressions: new FormControl('',),
        status: new FormControl('', <any>Validators.required)
      });


    }
  }

  get f() { return this.brandOwnerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.brandOwnerForm.invalid) {
      return;
    } else {

      this.router.navigate(['/campaign-list']);

    }
  }


  cancelForm = function () {
    this.router.navigate(['/campaign-list']);
  }

 

}

