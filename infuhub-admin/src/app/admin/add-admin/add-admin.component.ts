import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../services/admin.service';
import { CommonService } from '../services/common.service';
import { Observable } from "rxjs/Observable";
@Component({
    selector: 'app-add-admin',
    templateUrl: './add-admin.component.html',
    styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {
    //var status ="Active" ;
    //nrSelect:string = "Active" 
    adminEmailData={};
    allRoleData;
    addadminForm: FormGroup;
    submitted = false;
    isPassword = true;
    isCon_Password = true;

    id;
    admin_role_type="";
    fname;
    lname;
    email;
    status;
    action_name;
  
    constructor(private adminService: AdminService,private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
    ngOnInit() {
        this.status = 'Active';
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });

        this.allRoleData = [{"id":1,"role_name":"Super Admin","admin_type":"super-admin","status":"Active","created_at":"2019-02-05T12:05:55.000Z","updated_at":"2019-02-05T18:30:00.000Z","created_by":0,"updated_by":0},{"id":5,"role_name":"Sub Admin","admin_type":"sub-admin","status":"Active","created_at":"2019-02-07T09:50:20.000Z","updated_at":"2019-02-07T09:50:20.000Z","created_by":0,"updated_by":0},{"id":6,"role_name":"Creator","admin_type":"creator","status":"Active","created_at":"2019-02-07T10:11:29.000Z","updated_at":"2019-02-07T10:11:29.000Z","created_by":0,"updated_by":0}];

        if (this.id > 0) {
            this.action_name='Edit';
            this.isPassword = false;
            this.isCon_Password = false;
            this.addadminForm = new FormGroup({
                id: new FormControl(''),
                admin_role_type: new FormControl(''),                
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                password: new FormControl(''),
                cpassword: new FormControl(''),
                email: new FormControl('', <any>[Validators.required, Validators.email]),
                status: new FormControl('', <any>Validators.required)
            });
        } else {
            this.action_name='Edit';
            this.isPassword = true;
            this.isCon_Password = true;
            this.addadminForm = new FormGroup({
                id: new FormControl(''),
                admin_role_type: new FormControl('',<any>Validators.required), 
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                password: new FormControl('', <any>Validators.required),
                cpassword: new FormControl('', <any>[Validators.required]),
                email: new FormControl('', <any>[Validators.required, Validators.email]),
                status: new FormControl('', <any>Validators.required)
            });
        }
    }
    // validateAreEqual('password');
    get f() { return this.addadminForm.controls; }
    get password() { return this.addadminForm.get('password'); }
    get cpassword() { return this.addadminForm.get('cpassword'); }
    onSubmit() {
        this.submitted = true;
        if (this.addadminForm.invalid) {
            return;
        } else {
            this.router.navigate(['/admin-user']);          
        }
    }
    cancelForm = function () {
        this.router.navigate(['/admin-user']);
    }
    
   
}
