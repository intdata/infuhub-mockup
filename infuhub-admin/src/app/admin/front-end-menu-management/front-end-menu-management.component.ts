import { Component, OnInit,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { AdminMenuService } from '../services/admin-menu.service';
import { FrontMenuService } from '../services/front-menu.service';

@Component({
  selector: 'app-front-end-menu-management',
  templateUrl: './front-end-menu-management.component.html',
  styleUrls: ['./front-end-menu-management.component.scss']
})
export class FrontEndMenuManagementComponent implements OnInit,OnDestroy,AfterViewInit {
  menuData:any[]=[];
	menuGroupData:any[]=[];
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtOptions: any = {};
	dtTrigger: Subject<any> = new Subject();
	

	constructor(private frontMenuService:FrontMenuService, private router:Router) { }

	ngOnInit() {
		this.menuData = [{"id":100,"menu_position":0,"menu_label":"blog","slug":"blog","parent_id":0,"menu_link":"http://localhost:4200/admin/add-front-menu","menu_link_type":1,"menu_order":1,"menu_icon":"icon-new fa fa","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0},{"id":107,"menu_position":0,"menu_label":"Influencer","slug":"influencer","parent_id":0,"menu_link":"help","menu_link_type":1,"menu_order":2,"menu_icon":"undefined","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0},{"id":108,"menu_position":0,"menu_label":"Job","slug":"job","parent_id":107,"menu_link":"http://localhost:4200/admin/job","menu_link_type":1,"menu_order":1,"menu_icon":"icon-new fa fa","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0},{"id":109,"menu_position":0,"menu_label":"Job Details","slug":"job-details","parent_id":107,"menu_link":"http://localhost:4200/admin/job-details","menu_link_type":1,"menu_order":2,"menu_icon":"icon-new fa fa","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0},{"id":111,"menu_position":0,"menu_label":"blog-details","slug":"blog-details","parent_id":100,"menu_link":"http://localhost:4200/admin/add-front-menu","menu_link_type":1,"menu_order":1,"menu_icon":"icon-new fa fa","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0},{"id":112,"menu_position":0,"menu_label":"About Us","slug":"about-us","parent_id":0,"menu_link":"http://localhost:4200/admin/add-front-menu","menu_link_type":1,"menu_order":1,"menu_icon":"icon-new fa fa","status":"Active","created_at":null,"updated_at":null,"created_by":0,"updated_by":0}];
		
		this.dtOptions = {
			pageLength: 10,	  
			columnDefs : [
			    {
			        // Target the actions column
			        targets           : [5],
			        responsivePriority: 1,
			        filterable        : false,
			        sortable          : false,
			        orderable: false
			    }                
			]
		};
	}

	ngAfterViewInit(): void {

		
		this.dtTrigger.next();
		
	}	

	ngOnDestroy(): void {
		this.dtTrigger.unsubscribe();
	}

	
	deleteMenu = function(id){
		this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
		if(confirm("Are you sure to delete Menu? ")) {
		   this.frontMenuService.deleteMenu(id).subscribe(data => { 
			   if(data.msgcode==1 && data.results.affectedRows >0)
				{         
					dtInstance.destroy();
					this.ngAfterViewInit();
			   }
			 });
		   }
		});
		} 

		changeStatus = function(status){
    
			jQuery('#chnageStatusId').text('Inactive');
			// this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// if(confirm("Are you sure to delete Menu? ")) {
			// 	 this.frontMenuService.deleteMenu(id).subscribe(data => { 
			// 		 if(data.msgcode==1 && data.results.affectedRows >0)
			// 		{         
			// 			dtInstance.destroy();
			// 			this.ngAfterViewInit();
			// 		 }
			// 	 });
			// 	 }
			// });



			} 
		
		

}

