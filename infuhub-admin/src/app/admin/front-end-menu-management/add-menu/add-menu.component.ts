import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { FrontMenuService } from '../../services/front-menu.service';
@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {
  isMenuLinkOpen = false;
  isPageListOpen = false;
  submitted = false;
  addmenuForm: FormGroup;
  id: number;
  menu_position;
  menu_name: any;
  menu_link: any;
  page_list_name = "";
  link_type: any;
  menu_order: any;
  status = "";
  parent_id = 0;
  menu_icon_class: any;
  isReadOnly: boolean;
  menuGroupData: any;
  allPagesData: any;
  header_position = 0;
  footer_position = 0;
  link_type_external = 0;
  link_type_internal = 0;
  actionName;
  constructor(private frontMenuService: FrontMenuService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id > 0) {
      this.actionName = 'Edit';
      this.addmenuForm = this.formBuilder.group({
        id: new FormControl(''),
        menu_position: new FormControl('', [Validators.required]),
        parent_id: new FormControl(''),
        menu_name: new FormControl('', <any>Validators.required),
        menu_link: new FormControl(''),
        page_list_name: new FormControl(''),
        link_type: new FormControl('', <any>Validators.required),
        menu_order: new FormControl('', <any>Validators.required),
        menu_icon_class: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    } else {
      this.actionName = 'Add';
      this.addmenuForm = this.formBuilder.group({
        id: new FormControl(''),
        menu_position: new FormControl('', [Validators.required]),
        parent_id: new FormControl(''),
        menu_name: new FormControl('', <any>Validators.required),
        menu_link: new FormControl('', <any>Validators.required),
        page_list_name: new FormControl('', <any>Validators.required),
        link_type: new FormControl('', <any>Validators.required),
        menu_order: new FormControl('', <any>Validators.required),
        menu_icon_class: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    }
    
    //==================== get all CMS Pages======================//
   
    if (this.id > 0) {
      this.isReadOnly = true;
     
    }
    else {
      this.id = 0;
    }
   
  }
  get f() {
    return this.addmenuForm.controls;
  };
  onSubmit() {
    if (this.addmenuForm.get('link_type').value == 2) {
      this.addmenuForm.controls['menu_link'].clearValidators();
      this.addmenuForm.get('menu_link').updateValueAndValidity();
    } else {
      this.addmenuForm.controls['page_list_name'].clearValidators();
      this.addmenuForm.get('page_list_name').updateValueAndValidity();
    }
    this.submitted = true;
    if (this.addmenuForm.invalid) {
      return false;
    }
    else {
      this.router.navigate(['/front-menu']);
     
    }
  }
  Cancel = function () {
    this.router.navigate(['/front-menu']);
  }
  checkLinkType = function (value: number) {
    if (value == 1) {
      this.isPageListOpen = false;
      this.isMenuLinkOpen = true;
    } else if (value == 2) {
      this.isPageListOpen = true;
      this.isMenuLinkOpen = false;
    } else {
      this.isPageListOpen = false;
      this.isMenuLinkOpen = false;
    }
  }
}
