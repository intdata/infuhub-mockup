import { Component, OnInit } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { AdminMenuService } from '../services/admin-menu.service';

@Component({
  selector: 'app-admin-role-permission',
  templateUrl: './admin-role-permission.component.html',
  styleUrls: ['./admin-role-permission.component.scss']
})
export class AdminRolePermissionComponent implements OnInit {

  constructor(private adminMenuService:AdminMenuService, private activatedRoute: ActivatedRoute) { }
  data = {};
  permissionData = [];
  menuPermissionData;
  roleId;
  roleTypeName: any;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
            this.roleId = params['id'];
        });  
    this.menuPermissionData =   [{"id":25,"menu_id":2,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Influencer","menu_slug":"creator"},{"id":26,"menu_id":3,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Brand Owner","menu_slug":"brand-owner"},{"id":27,"menu_id":4,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Admin User","menu_slug":"admin-user"},{"id":29,"menu_id":6,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"CMS List","menu_slug":"cms"},{"id":31,"menu_id":8,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Faq List","menu_slug":"faq"},{"id":33,"menu_id":10,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Menu Management","menu_slug":"menu"},{"id":34,"menu_id":11,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":"Site Settings","menu_slug":"sitesettings"},{"id":35,"menu_id":12,"role_id":6,"permission_view":1,"permission_add":1,"permission_edit":0,"permission_delete":0,"created_at":null,"updated_at":null,"created_by":0,"updated_by":0,"menu_name":" Role Management","menu_slug":"admin-role"}];
   

    this.adminMenuService.getAdminRoleTypeName(this.roleId).subscribe(data=>{
      //console.log(data.results.role_name);
      this.roleTypeName = data.results.role_name;                
    });
  }
  //====================================//
  permission(menu_id,action,event){
  	this.data['role_id']      = this.roleId;
  	this.data['menu_id']       = menu_id;
    this.data['permission_action']  = action;
    if(event.target.checked == true){
      this.data['is_check'] = 1;
    }
    else{
      this.data['is_check'] = 0;
    }
  	this.adminMenuService.rolePermission(this.data).subscribe(data=>{});  	
   
  }

}

