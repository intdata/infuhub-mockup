import { Component, OnInit ,OnDestroy ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { CreatorService } from '../services/creator.service';
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables'; 
declare var $: any;
@Component({
  selector: 'app-creator-list',
  templateUrl: './creator-list.component.html',
  styleUrls: ['./creator-list.component.scss']
})
export class CreatorListComponent implements OnInit,OnDestroy,AfterViewInit {

 // #ViewChild("dataTable");
  creatorData:any[] = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();


  constructor(private creatorService:CreatorService, private router:Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.creatorData = [{"id":1,"platform_id":1,"email":"gt11@gmail.com","profile_img":"face1.jpg", "company_name": "Nexa Capital Pvt Ltd", "contact_No": "9831403332","first_name":"ginku4","last_name":"tinku1","status":"Inactive","created_at":"2019-01-14T14:38:37.000Z"},{"id":2,"platform_id":3, "profile_img":"face2.jpg","email":"rit@c.com", "company_name": "Sprainsces Alience Ltd Pvt Ltd", "contact_No": "9890403332","first_name":"utpal","last_name":"poulik","status":"Active","created_at":"2019-01-15T07:27:08.000Z"},{"id":3,"platform_id":2, "profile_img":"face3.jpg","email":"creator@one.com","company_name": "Alience Pvt Ltd", "contact_No": "8890404332","first_name":"Creator","last_name":"One","status":"Inactive","created_at":"2019-01-18T11:48:20.000Z"}];
    });
   this.dtOptions = {
     //pagingType: 'full_numbers',
      pageLength: 10,   
          columnDefs : [
              {
                  // Target the actions column
                  targets           : [0,7],
                  responsivePriority: 1,
                  filterable        : false,
                  sortable          : false,
                  orderable: false
              }                
          ]

      //processing: true
    };     
}

  ngAfterViewInit(): void { 
    
    this.dtTrigger.next();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
          const that = this;
          $('input', this.footer()).on('keyup change', function () {
              if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
              }
          });
      });
  });
   

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteCreator = function(id){

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      if(confirm("Are you sure to delete influencer ? ")) {

         this.creatorService.deleteCreatorData(id).subscribe(data => {
            if(data.msgcode==1 && data.results.affectedRows >0)
            {
         
             dtInstance.destroy();
             this.ngAfterViewInit();
           }
          });
      }

    });

  } 

}
