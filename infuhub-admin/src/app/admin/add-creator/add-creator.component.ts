import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { CreatorService } from '../services/creator.service';
import { NgFlashMessageService } from 'ng-flash-messages';
@Component({
    selector: 'app-add-creator',
    templateUrl: './add-creator.component.html',
    styleUrls: ['./add-creator.component.scss']
})
export class AddCreatorComponent implements OnInit {
    //nrSelect:string = "Active" 
    addcreatorForm: FormGroup;
    submitted = false;
    isPassword = true;
    id;
    platform;
    fname;
    lname;
    email;
    company_name;
    contact_no;
    status;

    action_name;

    constructor(private creatorService: CreatorService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,private ngFlashMessageService: NgFlashMessageService) {}

    ngOnInit() {
        this.platform = 1;
        this.status = 'Active';
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        if (this.id > 0) {

           
            // this.creatorService.getSingleCreatorData(this.id).subscribe(data => {
               
            //     if (data.msgcode == 1 && data.results.length > 0) {
            //         this.id = data.results[0].id;
            //         this.platform = data.results[0].platform_id;
            //         this.fname = data.results[0].first_name;
            //         this.lname = data.results[0].last_name;
            //         this.email = data.results[0].email;
            //         this.status = data.results[0].status;
            //     }
            // });
            

        }
        if (this.id > 0) {

            this.action_name='Edit';

            this.isPassword = false;
            this.addcreatorForm = new FormGroup({
                id: new FormControl(''),
                platform: new FormControl(''),
                fname: new FormControl('', < any > Validators.required),
                lname: new FormControl('', < any > Validators.required),
                password: new FormControl(''),
                email: new FormControl('', < any > [Validators.required, Validators.email]),
                company_name: new FormControl(''),
                contact_no: new FormControl('', < any > [Validators.required,Validators.pattern('[0-9]+')]),
                status: new FormControl('', < any > Validators.required)
            });
        }
        else {

            this.action_name='Add';
            this.isPassword = true;
            this.addcreatorForm = new FormGroup({
                id: new FormControl(''),
                platform: new FormControl(''),
                fname: new FormControl('', < any > Validators.required),
                lname: new FormControl('', < any > Validators.required),
                password: new FormControl('', < any > Validators.required),
                email: new FormControl('', < any > [Validators.required, Validators.email]),
                company_name: new FormControl(''),
                contact_no: new FormControl('', < any > [Validators.required,Validators.pattern('[0-9]+')]),
                status: new FormControl('', < any > Validators.required)
            });
        }
    }

    get f() {
        return this.addcreatorForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        if (this.addcreatorForm.invalid) {
            return;
        }
        else {

            this.ngFlashMessageService.showFlashMessage({
                                // Array of messages each will be displayed in new line
                                messages: ["Influencer has been added succefully"], 
                                // Whether the flash can be dismissed by the user defaults to false
                                dismissible: true, 
                                // Time after which the flash disappears defaults to 2000ms
                                timeout: false,
                                // Type of flash message, it defaults to info and success, warning, danger types can also be used
                                type: 'success'
                              });

            this.router.navigate(['/influencer']);

            // if (this.id > 0) {
            //     this.creatorService.editCreatorData(this.addcreatorForm.value).subscribe(data => {
            //         if (data.msgcode == 1 && data.results.affectedRows > 0) {
            //             this.router.navigate(['/creator']);
            //         }
            //     });
            // }
            // else {
            //     this.creatorService.saveCreatorData(this.addcreatorForm.value).subscribe(data => {
            //         if (data.msgcode == 1 && data.results.affectedRows > 0) {

            //             this.ngFlashMessageService.showFlashMessage({
            //                 // Array of messages each will be displayed in new line
            //                 messages: ["Influencer has been added succefully"], 
            //                 // Whether the flash can be dismissed by the user defaults to false
            //                 dismissible: true, 
            //                 // Time after which the flash disappears defaults to 2000ms
            //                 timeout: false,
            //                 // Type of flash message, it defaults to info and success, warning, danger types can also be used
            //                 type: 'success'
            //               });

            //             this.router.navigate(['/creator']);
            //         }
            //     });
            // }
        }
    }
    
    cancelForm = function() {
        this.router.navigate(['/influencer']);
    }
}
