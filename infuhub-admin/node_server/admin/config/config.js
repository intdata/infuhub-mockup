//var _config = {}
//_config.PORT                = 8000;
//exports.CONF_VAR = _config;



module.exports = {
    port : 8001,
    host : 'localhost',

	DB_CON : {
        user: 'root',
        password: 'root',
        server: 'localhost',
        database: 'infuhub',
        options: {
            encrypt: true
        }
    },

    slugify : function (string) {

          const a = 'àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
          const b = 'aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
          const p = new RegExp(a.split('').join('|'), 'g')

          return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with ‘and’
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple — with single -
            .replace(/^-+/, '') // Trim — from start of text .replace(/-+$/, '') // Trim — from end of text
},
mysql_real_escape_string:function(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

    
   
}


