var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var crypto = require('crypto');
var async = require('async');
//===============check Email Id Exit Or Not========================================//

module.exports = {
	check_duplicate_slug: function (string, callback) {
		var data = {};
		var slug = config.slugify(string);

		var query_slug = "SELECT slug FROM front_end_menu_master WHERE menu_label='" + string + "'  ORDER BY id DESC LIMIT 1";
		db_connection.con.query(query_slug, function (err, result) {
			if (result.length > 0) {
				var last_slug_value = result[0].slug;
				var stringSplit = last_slug_value.split('-');
				var checklastStringIsnumeric = stringSplit[stringSplit.length - 1];
				if (!isNaN(checklastStringIsnumeric)) {
					var count = parseInt(checklastStringIsnumeric) + 1;
					slug = config.slugify(string) + '-' + count;
				} else {
					slug = config.slugify(string) + '-1';
				}
			}
			var query_slug_exit = "SELECT count(*) as countSlug FROM front_end_menu_master WHERE slug='" + slug + "'";
			db_connection.con.query(query_slug_exit, function (err, result) {
				if (result[0].countSlug > 0) {
					slug = slug + '-1';
				}
				callback(slug);
			});
		});
	},
	get_menu_tree1: function (parent_id, callback) {
		sub_mark = "";
		var cat = get_menu_tree(parent_id, sub_mark, function (response) {
			callback(response);
		
		});
	
	}

};
 
function get_menu_tree(parent_id, sub_mark = '', callback) {
	var data = {};
	var menu = "";
	var count = 0;
	var query_menu_tree = "";
	
	query_menu_tree = "SELECT * FROM front_end_menu_master where status='Active' and parent_id='" + parent_id + "'";
	db_connection.con.query(query_menu_tree, function (err, result) {
		var d = 1;

		if (!!err) {
			callback(err);
		} else {

			if (result.length > 0) {

				var total =result.length;
                 
				//console.log(index +'recu'+count);
				for (var i = 0; i < total; i++) {
						menu += '<option value="' + result[i]['id'] + '">' + sub_mark + result[i]['menu_label'] + '</option>';
						
						get_menu_tree(result[i]['id'], sub_mark + '--', callback)
						
				}

				callback(menu);
			}

		}

	

	});

}

