var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var crypto = require('crypto');

router.post('/saveBrandOwner/', function (req, res) {
	var data = {};

	//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;

	//================Password Hashing=================================//

	var password = req.body.password;
	var hashPassword = crypto.createHash('md5').update(password).digest('hex');

	//================================Create Sub Admin==============================//   

	var query = "INSERT INTO brand_owner (comapany_name, company_ph_no,email,password,status,created_at,updated_at) VALUES ('" + req.body.comapany_name + "','" + req.body.company_ph_no + "','" + req.body.email + "','" + hashPassword + "','" + req.body.status + "','" + dateTime + "','" + dateTime + "')";

	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

			res.send(data);
		} else {

			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



router.post('/editBrandOwner/', function (req, res) {
	var data = {};

	//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;

	//================================Edit Sub Admin==============================//   

	var query = "UPDATE brand_owner SET comapany_name ='" + req.body.comapany_name + "',company_ph_no ='" + req.body.company_ph_no + "',email='" + req.body.email + "',status='" + req.body.status + "', updated_at='" + dateTime + "' WHERE id='" + req.body.id + "'";


	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});

});

router.get('/deleteBrandOwner/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var UserId = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM brand_owner WHERE brand_owner.id = '" + UserId + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/getAllBrandOwnerData/', function (req, res) {
	var data = {};

	var query = "SELECT id,comapany_name,company_ph_no,email,status,created_at FROM brand_owner Order By updated_at Desc";
	//console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



router.get('/getEditData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var UserId = _get['id'];
	//console.log(req.query);
	var query = "SELECT id,email,comapany_name,company_ph_no,status,created_at FROM brand_owner WHERE id='" + UserId + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});

//===============check Email Id Exit Or Not========================================//
router.post('/checkEmailExitsOrNot/', function (req, res) {
	var data = {};
	var custParams = "";
	var TableName = "";
	if (req.body.id > 0) {
		custParams = ' AND id!=' + req.body.id;
	}

	TableName = req.body.tablename;
	var query = "SELECT email FROM "+ TableName +" WHERE email='" + req.body.email + "'" + custParams;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {

			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

		} else {

			if (result.length > 0) {
				data["statusCode"] = 400;
				data["error"] = 'Bad Request';
				data["message"] = 'Email address already registerd';

				res.status(400).send(data);
			} else {
				res.status(200).send('null')
			}

			//console.log(data);            
		}
	});

});


module.exports = router;
