var config  = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection  = require('../config/db_connection');
var crypto = require('crypto');

router.post('/saveCreator/', function(req, res) {	
	var data = {};
			
	//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); 
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;

	//================Password Hashing=================================//

    var password = req.body.password;
    var hashPassword = crypto.createHash('md5').update(password).digest('hex');
       
    //================================Create Sub Admin==============================//   

	var query="INSERT INTO influencer (platform_id,first_name,last_name,email,password,status,created_at,updated_at) VALUES ('"+req.body.platform+"','"+req.body.fname+"','"+req.body.lname+"','"+req.body.email+"','"+hashPassword+"','"+req.body.status+"','"+dateTime+"','"+dateTime+"')";
    
    db_connection.con.query(query, function (err, result) {

		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;

			res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;
			res.send(data);
		}
	});
});



router.post('/editCreator/', function(req, res) {	
	var data = {};
			
			//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); 
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;

    //================================Edit Sub Admin==============================//   

    var query = "UPDATE influencer SET platform_id='"+req.body.platform+"',first_name='"+req.body.fname+"',last_name='"+req.body.lname+"',email='"+req.body.email+"',status='"+req.body.status+"', updated_at='"+dateTime+"' WHERE id='"+req.body.id+"'"; 
    

    db_connection.con.query(query, function (err, result) {

		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;

			res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;

			res.send(data);
		}
	});

});

router.get('/deleteCreator/:id', function(req, res) {
	var data = {};
	var _get 		= req.params;	
	var UserId       = _get['id'];
	//console.log(req.query);
	var query="DELETE FROM influencer WHERE influencer.id = '"+UserId+"'";
	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/getAllCreatorData/', function(req, res) {
	var data = {};

	var query="SELECT id,platform_id,email,first_name,last_name,status,created_at FROM influencer";
	//console.log(query);
	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;
			res.send(data);
		}
	});
});



router.get('/getEditData/:id', function(req, res) {
	var data = {};
	var _get 		= req.params;	
	var UserId       = _get['id'];
	//console.log(req.query);
    var query="SELECT id,platform_id,email,first_name,last_name,status,created_at FROM influencer WHERE id='"+UserId+"'";
	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;
			res.send(data);
		}
	});
});

 
module.exports = router;
