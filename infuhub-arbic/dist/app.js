process.on('uncaughtException', function (err) {
  console.log("Execption : Node Exit Prevented ", err);
});


var express = require('express');
var path = require('path');
var http = require('http');


var bodyParser = require('body-parser');
//var config= require('./config/config').CONF_VAR;
var config = require('./admin/config/config');
////////////////////router file start////////////////////////////////////////////
var sspl 				= require('./admin/routes/sspl');
var common_function 				= require('./admin/routes/common_function');
var adminuser 			= require('./admin/routes/adminuser');
var creator 			= require('./admin/routes/creator');
var brand_owner 		= require('./admin/routes/brand_owner');
var admincms 			= require('./admin/routes/admincms');
var adminmenu 			= require('./admin/routes/adminmenu');
var faq 			= require('./admin/routes/faq');
var front_end_menu 			= require('./admin/routes/front_end_menu');
var campaign 			= require('./admin/routes/campaign');
var email_template 			= require('./admin/routes/admin_email_template');
var testimonial 			= require('./admin/routes/testimonial');

////////////////////////////router file start //////////////////
var app = express();
/////////////////// Passing db Object for router ends//////////////////////////
app.use(function (req, res, next) {        
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');    
    res.setHeader('Access-Control-Allow-Origin', 'http://10.0.4.133:4200');  
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');      
    res.setHeader('Access-Control-Allow-Credentials', true);      
    next();  
});

//================= body parser===================//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//=================================================//

app.use('/sspl', sspl);
app.use('/common_function', common_function);
app.use('/adminuser', adminuser);
app.use('/creator', creator);
app.use('/brand_owner', brand_owner);
app.use('/admincms', admincms);
app.use('/adminmenu', adminmenu);
app.use('/faq', faq);
app.use('/front_end_menu', front_end_menu);
app.use('/campaign', campaign);
app.use('/email_template', email_template);
app.use('/testimonial', testimonial);



app.listen('8000');
//app.listen(app.get('port'));
console.log('server start running at port '+ config.port);
module.exports = app;	
