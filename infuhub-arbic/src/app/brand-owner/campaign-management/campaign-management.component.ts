import { Component, OnInit } from '@angular/core';
import { AstMemoryEfficientTransformer } from '@angular/compiler';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-campaign-management',
  templateUrl: './campaign-management.component.html',
  styleUrls: ['./campaign-management.component.scss']
})
export class CampaignManagementComponent implements OnInit {

  public show: boolean = true;
  public show2: boolean = false;
  public show3: boolean = false;
  public show4: boolean = false;
  public show5: boolean = false;
  public totalCompareArr:Array<any> = [];

  public social = {
    fbChecked : false,
    twChecked : false,
    uChecked : false,
    inChecked : false,
  };
  objectKeys = Object.keys;

  public active = false;
  public togglee: boolean = false;
  public togglee2: boolean = false;
  public togglee3: boolean = false;
  public togglee4: boolean = false;



  constructor(
    private route : ActivatedRoute
  ) { }

  ngOnInit() {
    const totalCompareCount = Number(this.route.snapshot.paramMap.get("length"));
    for (let i = 0; i < totalCompareCount; i++) {
      this.totalCompareArr.push({
        campaign: false
      });
    }
  }

  toggle() {
    this.show = false;
    this.show2 = true;
    this.show3 = false;
    this.show4 = false;
    this.togglee = true;
    this.togglee2 = false;
    this.togglee3 = false;
  }
  toggle2() {
    this.show = false;
    this.show2 = false;
    this.show3 = true;
    this.show4 = false;
    this.active = true;
    this.togglee = true;
    this.togglee2 = true;
    this.togglee3 = false;
  }
  toggle3() {
    this.show = false;
    this.show2 = false;
    this.show3 = false;
    this.show4 = true;
    this.togglee = true;
    this.togglee2 = true;
    this.togglee3 = true;
  }

  goToaddInfluners() {
    this.show = false;
    this.show2 = false;
    this.show3 = false;
    this.show4 = false;
    this.show5 = true;
    this.togglee = true;
    this.togglee2 = true;
    this.togglee3 = true;
    this.togglee4 = true;
  }

  back1() {
    this.show = true;
    this.show2 = false;
    this.show3 = false;
    this.show4 = false;
    this.togglee = false;
    this.togglee2 = false;
    this.togglee3 = false;
  }

  back2() {
    this.show = false;
    this.show2 = true;
    this.show3 = false;
    this.show4 = false;
    this.togglee = true;
    this.togglee2 = false;
    this.togglee3 = false;
  }
  onOpenChange(t) {
    console.log(this.social)
  }

  objectLength(obj) {
    return Object.keys(obj);
  }

}
