import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';


@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss']
})
export class CampaignDetailsComponent implements OnInit {

  investerList: any;
  followerList: any;
  likeList:any;
  soacialList:any;

  @ViewChild('doughnutCanvasA') doughnutCanvasA:ElementRef
  @ViewChild('doughnutCanvas1') doughnutCanvas1:ElementRef

  // doughnut
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = false;
  public doughnutChartPlugins = [];

  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[];
  public doughnutChartData: SingleDataSet;

  public doughnutChartOptions1: ChartOptions = {};
  public doughnutChartLabels1: Label[];
  public doughnutChartData1: SingleDataSet;

  public doughnutChartOptions2: ChartOptions = {};
  public doughnutChartLabels2: Label[];
  public doughnutChartData2: SingleDataSet;

  // area line chart
  public lineChartData: ChartDataSets[];
  public lineChartData1: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    elements: {
        line: {
            tension: 0
        }
    },
    scales:{
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#79d6bd',
      backgroundColor: 'rgba(121, 214, 189, 0.3)',
    },
  ];
  public lineChartColors1: Color[] = [
    {
      borderColor: '#f79636',
      backgroundColor: 'rgba(252, 187, 174,0.3)',
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  // bar chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    scales:{
      xAxes: [{
        barThickness: 20,
        gridLines: {
            offsetGridLines: true
        }
    }],
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public barDataColors: Color[];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  public barChartData: ChartDataSets[];
  public barChartLabels: Label[];


  @ViewChild('doughnutCanvas3') doughnutCanvas3:ElementRef

  // doughnut
  public doughnutChartOptions3: ChartOptions = {};
  public doughnutChartLabels3: Label[] = ['Ongoing campaigns', 'Total campaigns'];
  public doughnutChartData3: SingleDataSet;
  public doughnutDataColors3: any[];
  public doughnutChartType3: ChartType = 'doughnut';
  public doughnutChartLegend3 = false;
  public doughnutChartPlugins3 = [];


  constructor() { }

  ngOnInit() {
    // doughnut
    let me = this;
    this.doughnutChartOptions = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled:false
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };
    this.doughnutDataColors = [{ backgroundColor: ['rgba(30, 169, 224, 0.8)', 'rgba(255,165,0,0.9)']}];

    // doughnut chart for Total Amount Invested
    var ctx = this.doughnutCanvasA.nativeElement.getContext("2d");
    this.doughnutChartData = [20, 80];
    this.doughnutChartLabels = ['%Pending', '% Of Amount Invested'];
    this.doughnutChartOptions.animation = { 
      //duration: 0,
      onComplete: function() {
        console.log(me.doughnutCanvasA)
        me.centerLabel(ctx, "20%", "OF $100K", "#45a5e0", "#F29D45", me.doughnutCanvasA);
      }
    }

    // doughnut chart for Individual Return
    var ctx1 = this.doughnutCanvas1.nativeElement.getContext("2d");
    this.doughnutChartData1 = [20, 80];
    this.doughnutChartLabels1 = ['Amount Invested', 'Return'];
    this.doughnutChartOptions1 = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled:false
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };
    this.doughnutChartOptions1.animation = { 
      //duration: 0,
      onComplete: function() {
        me.centerLabel(ctx1, "$120K", "ON $80K", "#F29D45", "#45a5e0", me.doughnutCanvas1);
      }
    }


    // line chart labels
    this.lineChartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

    // line chart for followers reached
    this.lineChartData = [
      { data: [168, 176, 228, 260], label: 'Followers Reached' },
    ];
    
    // line chart for like & comments
    this.lineChartData1 = [
      { data: [57, 123, 210, 234], label: 'Likes & Comments' },
    ];

    // bar chart for platform comparison
    this.barChartLabels = ['FB', 'TWEET', 'YOUTUBE', 'INSTAGRAM', 'SNAPCHAT'];
    this.barChartData = [
      { data: [125, 110, 90, 80, 50, 20], label: 'Platform Comparison' }
    ];
    this.barDataColors = [{ 
        backgroundColor: [
          '#d5e7f9', 
          '#c4ebf7', 
          '#f8d2cc',
          '#f8c9c4',
          '#f7ecc8'
        ]
      }];

    this.investerList=[
      {
        img: 'infl-search-pic1.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '3k'
      },
      {
        img: 'infl-search-pic2.jpg',
        name: 'Jasmin M',
        date: '02nd Apr',
        amount: '3k'
      },
      {
        img: 'infl-search-pic3.jpg',
        name: 'Amelia G',
        date: '02nd Apr',
        amount: '3k'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '3k'
      },
      {
        img: 'infl-search-pic5.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '3k'
      }
    ],
    this.followerList=[
      {
        img: 'infl-search-pic1.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '11'
      },
      {
        img: 'infl-search-pic2.jpg',
        name: 'Jasmin M',
        date: '02nd Apr',
        amount: '3'
      },
      {
        img: 'infl-search-pic3.jpg',
        name: 'Amelia G',
        date: '02nd Apr',
        amount: '33'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '44'
      },
      {
        img: 'infl-search-pic5.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '10'
      }
    ],
    this.likeList=[
      {
        img: 'infl-search-pic1.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      },
      {
        img: 'infl-search-pic2.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '02',
        comment:'05'
      },
      {
        img: 'infl-search-pic3.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '21',
        comment:'15'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      }
    ],
    this.soacialList=[
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        social:'campaign-fb.png'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        social:'campaign-fb.png'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        social:'campaign-twitter.png'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        social:'campaign-utube.png'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        social:'campaign-twitter.png'
      }
    ]
  
    
    // doughnut 3
    var ctx3 = this.doughnutCanvas3.nativeElement.getContext("2d");
    this.doughnutChartData3 = [80, 20];
    this.doughnutDataColors3 = [{ backgroundColor: ['rgba(255,165,0,0.9)', 'rgba(30, 169, 224, 0.8)']}];
    
    this.doughnutChartOptions3 = {
      responsive: true,
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      cutoutPercentage: 80,
      tooltips:{
        enabled:false
      },
      animation:{ 
        //duration: 0,
        onComplete: function() {
          me.centerLabel2(ctx3, 'Male - 100', 'Female - 20', '#40a8f2', '#f19b3d', me.doughnutCanvas3);
        }
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };
  }

  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx, middleText1, middleText2, textColor1, textColor2, doughnutCanvas) {
       var width = doughnutCanvas.nativeElement.clientWidth,
           height = doughnutCanvas.nativeElement.clientHeight;

       var fontSize = (height / 50).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "middle";
       ctx.fillStyle = textColor1;

       var text = middleText1,
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 - 10;

       ctx.fillText(text, textX, textY, 200);

       var fontSize = (height / 110).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "middle";
       ctx.fillStyle = textColor2;

       var text = middleText2,
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 + 15;

       ctx.fillText(text, textX, textY, 200);
       ctx.restore();
   }

   /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel2(ctx , middleText1, middleText2, textColor1, textColor2, doughnutCanvas) {
    //   Chart.types.Doughnut.prototype.draw.apply(this, arguments);

       var width = doughnutCanvas.nativeElement.clientWidth,
           height = doughnutCanvas.nativeElement.clientHeight;

       var fontSize = (height / 100).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "top";
       ctx.fillStyle = textColor1;

       var text = middleText1,
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 - 10;

       ctx.fillText(text, textX, textY, 200);

       var fontSize = (height / 100).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "bottom";
       ctx.fillStyle = textColor2;

       var text1 = middleText2,
           textX = Math.round((width - ctx.measureText(text1).width) / 2),
           textY = height / 2 + 40; 

       ctx.fillText(text1, textX, textY, 200);
       ctx.restore();
   }

 

}
