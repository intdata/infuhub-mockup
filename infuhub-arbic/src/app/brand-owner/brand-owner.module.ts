import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { BrandOwnerRoutingModule } from './brand-owner-routing.module';
import { BrandOwnerLayoutComponent } from './brand-owner-layout/brand-owner-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { InfluencerSearchComponent } from './influencer-search/influencer-search.component';
import { InfluencerSavedComponent } from './influencer-saved/influencer-saved.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignManagementComponent } from './campaign-management/campaign-management.component';
import { MessagesComponent } from './messages/messages.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { NgbTabsetModule, NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
import { ChartsModule } from 'ng2-charts';
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { CampaignManagementDetailsComponent } from './campaign-management-details/campaign-management-details.component';
import { MessageDetailsComponent } from './message-details/message-details.component';


@NgModule({
  imports: [
    CommonModule,
    BrandOwnerRoutingModule,
    NgbTabsetModule,
    NgbDropdownModule,
    NgbTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule 
  ],
  declarations: [
    BrandOwnerLayoutComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    InfluencerSearchComponent,
    InfluencerSavedComponent,
    CampaignComponent,
    CampaignManagementComponent,
    MessagesComponent,
    HelpComponent,
    CompareInfluencersComponent,
    CampaignListComponent,
    ProfileComponent,
    NotificationsComponent,
    CampaignDetailsComponent,
    CampaignManagementDetailsComponent,
    MessageDetailsComponent
  ],
  providers:[
    
  ]

})
export class BrandOwnerModule { }
