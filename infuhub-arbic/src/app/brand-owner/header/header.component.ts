import { Component, OnInit } from '@angular/core';
import {smoothlyMenu} from "../app.helpers";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  toggleNavigation(): void {
    //jQuery("body").toggleClass("mini-navbar");
    smoothlyMenu();
}
logout() {
    localStorage.clear();
    // location.href='http://to_login_page';
}

}
