import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-influencer-search',
  templateUrl: './influencer-search.component.html',
  styleUrls: ['./influencer-search.component.scss']
})
export class InfluencerSearchComponent implements OnInit {

  public compareItems: Array<any>;
  public compareArr: Array<any>;
  public expandMode: boolean;
  public expandCampaign: boolean;
  @ViewChild('expanDiv') expanDiv: ElementRef;
  @ViewChild('expanFixd') expanFixd: ElementRef;
  constructor(
    private renderer: Renderer2,
    private route: Router
  ) {
    this.expandMode = false;
    this.expandCampaign = false;
  }

  ngOnInit() {
    this._staticCompareArray();
  }
  _staticCompareArray() {
    this.compareItems = [];
    this.compareArr = [];
    for (let i = 0; i < 4; i++) {
      this.compareItems.push({
        added: false,
        campaign: false,
      })
    }
  }

  addCompare(indx) {
    this.compareItems[indx].added = !this.compareItems[indx].added;
    this.compareArr = this.compareItems.filter(added => added.added);
    // console.log(this.compareItems.filter(added => added.added).length)
  }
  get totalCompare() {
    return this.compareItems.filter(added => added.added).length;
  }
  toggleSlide() {
    if (!this.expandMode) {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `0`);
      this.expandMode = true;
    } else {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `-225px`);
      this.expandMode = false;
    }
  }

  addCampaign(indx) {
    this.compareItems[indx].campaign = !this.compareItems[indx].campaign;
  }
  get totalCampaign() {
    return this.compareItems.filter(x => x.campaign).length;
  }

  openCampaign() {
    if (!this.expandCampaign && this.totalCampaign > 0) {
      this.renderer.addClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = true;
    } else {
      this.renderer.removeClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = false;
    }
  }

}
