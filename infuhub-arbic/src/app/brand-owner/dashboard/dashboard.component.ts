import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // Pie
  public pieChartOptions: ChartOptions = {};
  public pieChartLabels: Label[] = ['Ongoing campaigns', 'Published campaigns', 'Total created campaigns'];
  public pieChartData: SingleDataSet;
  public pieChartDataColors: any[];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  
  @ViewChild('doughnutCanvas') doughnutCanvas:ElementRef

  // doughnut
  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[] = ['Ongoing campaigns', 'Total campaigns'];
  public doughnutChartData: SingleDataSet;
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = false;
  public doughnutChartPlugins = [];

  constructor() { }

  ngOnInit() {
    // pie
    this.pieChartData = [80, 10, 10];
    this.pieChartDataColors = [
      {
        backgroundColor: [
          'rgba(30, 169, 224, 0.8)',
          'rgba(255,165,0,0.9)',
          'rgba(139, 136, 136, 0.9)',
        ]
      }
    ];
    this.pieChartOptions = {
      responsive: true,
      rotation: Math.PI / 2,
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    }

    // doughnut
    var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
    let me = this;
    this.doughnutChartData = [80, 20];
    this.doughnutDataColors = [{ backgroundColor: ['rgba(255,165,0,0.9)', 'rgba(30, 169, 224, 0.8)']}];
    
    this.doughnutChartOptions = {
      responsive: true,
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      cutoutPercentage: 80,
      tooltips:{
        enabled:false
      },
      animation:{ 
        //duration: 0,
        onComplete: function() {
          me.centerLabel(ctx);
        }
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };
  }

  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx) {
    //   Chart.types.Doughnut.prototype.draw.apply(this, arguments);

       var width = this.doughnutCanvas.nativeElement.clientWidth,
           height = this.doughnutCanvas.nativeElement.clientHeight;

       var fontSize = (height / 80).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "top";
       ctx.fillStyle = "grey";

       var text = "Total",
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 - 10;

       ctx.fillText(text, textX, textY, 200);

       var fontSize = (height / 90).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "bottom";
       ctx.fillStyle = "grey";

       var text1 = "600",
           textX = Math.round((width - ctx.measureText(text1).width) / 2),
           textY = height / 2 + 40; 

       ctx.fillText(text1, textX, textY, 200);
       ctx.restore();
   }

  // onGoingCampaigns: [
  //   {
  //     "campainName": "ABCRX",
  //     "influencerName": [
  //       'assets/front_end/images/testimonial_img.jpg',
  //       'assets/front_end/images/testimonial_img.jpg'
  //     ],
  //     "platForm": ""
  //   }
  // ]

}
