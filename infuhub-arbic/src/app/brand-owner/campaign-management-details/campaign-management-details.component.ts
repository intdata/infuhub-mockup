import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaign-management-details',
  templateUrl: './campaign-management-details.component.html',
  styleUrls: ['./campaign-management-details.component.scss']
})
export class CampaignManagementDetailsComponent implements OnInit {
  public progressArr: Array<any> = [];
  public selectedItemObj;
  constructor() { }

  ngOnInit() {
    this.staticProgressArr();
  }
  staticProgressArr = () => {
    let name = ['John Smith','Ruby','Jasmin M','Amelia G','Amanda C']
    for(let i=0; i<5; i++){
      this.progressArr.push({
        checked: false,
        name: name[i]
      })
      this.progressArr[0].checked = true;
      this.selectedItemObj = this.progressArr[0];
    }
  }
  selectItem(idx){
    this.selectedItemObj = this.progressArr[idx];
    this.progressArr.filter( x => x.checked = false);
    this.progressArr[idx].checked = true;
  }
}
