import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public show_dialog: boolean = false;
  public wasClicked: boolean = false;


  constructor(
    private router: Router,
    private activateRote :ActivatedRoute
  ) { }
  //$: any;
  ngOnInit() :void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.toggle('arbic');

// window.onscroll = function() {myFunction()};

// var header = document.getElementById("header");
// var sticky = header.offsetTop;

// function myFunction() {
//   if (window.pageYOffset > sticky) {
//     header.classList.add("fixed-header");
//   } else {
//     header.classList.remove("fixed-header");
//   }
// }

  }

  
  

  routeIsActive(routePath: string) {
    return  this.router.url == routePath;
 }


  toggle() {
    this.show_dialog = !this.show_dialog;
  }

  languageChange(){
    this.wasClicked = !this.wasClicked;
    // const body = document.getElementsByTagName('body')[0];
    // body.classList.toggle('arbic');
  }


}
