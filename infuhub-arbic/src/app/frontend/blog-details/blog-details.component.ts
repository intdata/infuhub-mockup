import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss']
})
export class BlogDetailsComponent implements OnInit {

  constructor() { }

  blogDetailsLike=[
    [
      'assets/front_end/images/blogdetails-pic3.jpg',
      '30 April 2019', '10:30PM',
      'Lorem ipsum dolor sit amet,consectetur',
      'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
    ],
    [
      'assets/front_end/images/blogdetails-pic4.jpg',
      '30 April 2019', '10:30PM',
      'Lorem ipsum dolor sit amet,consectetur',
      'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
    ],
    [
      'assets/front_end/images/blogdetails-pic5.jpg',
      '30 April 2019', '10:30PM',
      'Lorem ipsum dolor sit amet,consectetur',
      'Duis lorem ante, tempus non felis quis, malesuada porttitor quam',
    ]
  ]

  ngOnInit() {
  }

}
