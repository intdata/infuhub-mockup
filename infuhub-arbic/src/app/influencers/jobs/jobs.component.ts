import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  public jobsArr:Array<any> = [];
  constructor() { }

  ngOnInit() {
    for(let i=0; i<5; i++){
      this.jobsArr.push({
        statusText : 'Published'
      });
    }
  }
  statusMode(statusText,indx){
    this.jobsArr[indx].statusText = statusText;
  }

}
