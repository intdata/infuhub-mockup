import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  jobStatus = [
    {
      "title": "Total Ongoing Jobs",
      "date": "March 2019",
      "images": "assets/influencers/images/round-icon1.png",
      "progress": "In progress",
      "approval": "Content submitted for approval",
      "notStartes": "Content approved",
      "published": "Content is published",
      "paid": "Paid"
    },
    {
      "title": "Total Past Jobs",
      "date": "March 2019",
      "images": "assets/influencers/images/round-icon2.png",
      "progress": "On progress jobs",
      "approval": "Completed jobs",
      "notStartes": "Not started",
    },
    {
      "title": "Revenue status",
      "date": "March 2019",
      "images": "assets/influencers/images/round-icon3.png",
      "progress": "% Pending to collect",
      "approval": "% Collected",
    }
  ]

}
