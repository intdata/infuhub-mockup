import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfluencersRoutingModule } from './influencers-routing.module';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { InfluencersLayoutComponent } from './influencers-layout/influencers-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InvitationsComponent } from './invitations/invitations.component';
import { OpportunitiesComponent } from './opportunities/opportunities.component';
import { JobsComponent } from './jobs/jobs.component';
import { MessagesComponent } from './messages/messages.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { NgbTabsetModule, NgbModalModule, NgbDatepicker, NgbDatepickerModule, NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsComponent } from './notifications/notifications.component';
import { FormsModule } from '@angular/forms';
import { InvitationDetailsComponent } from './invitation-details/invitation-details.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  imports: [
    CommonModule,
    InfluencersRoutingModule,
    NgbTabsetModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbDropdownModule,
    NgbTooltipModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [
    HeaderComponent, 
    SidebarComponent, 
    InfluencersLayoutComponent, 
    DashboardComponent, 
    InvitationsComponent, 
    OpportunitiesComponent, 
    JobsComponent, 
    MessagesComponent, 
    HelpComponent, 
    CompareInfluencersComponent,
    NotificationsComponent,
    InvitationDetailsComponent,
    JobDetailsComponent,
    MyaccountComponent,
    MessageDetailsComponent
  ]
})
export class InfluencersModule { }
